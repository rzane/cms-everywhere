﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMSEverywhereLib
{
    public class GoogleAnalytics
    {
        public static DataSet getAllGA()
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getAllGA";
            command.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static DataSet getGAbyID(int gaID)
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getGAbyID";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter gaIDparam = new SqlParameter("@gaID", SqlDbType.Int);
            gaIDparam.Direction = ParameterDirection.Input;
            gaIDparam.Value = gaID;
            command.Parameters.Add(gaIDparam);

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static bool addGALoginInfo(string gaLogin, string gaPass, out string errorMessage)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_addGALoginInfo";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@gaLogin", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = gaLogin;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@gaPass", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = gaPass;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@errorMessage", SqlDbType.NVarChar, 100);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            errorMessage = (string)command.Parameters["@errorMessage"].Value;

            return succeeded;
        }

        public static bool updateGALoginInfo(int gaID, string gaLogin, string gaPass)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_updateGALoginInfo";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@gaID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = gaID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@gaLogin", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = gaLogin;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@gaPass", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = gaPass;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            return succeeded;
        }

        public static bool deleteGALoginInfo(int gaID, out string errorMessage)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_deleteGALoginInfo";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@gaID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = gaID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@errorMessage", SqlDbType.NVarChar, 100);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            errorMessage = (string)command.Parameters["@errorMessage"].Value;

            return succeeded;
        }

    }
}
