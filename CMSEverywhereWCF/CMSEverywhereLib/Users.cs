﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMSEverywhereLib
{
    public class Users
    {

        public static int GetUserID(string guidstring)
        {
            Guid aspnet_userID = new Guid(guidstring);


            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "procGetUserID";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@aspnet_userId", SqlDbType.UniqueIdentifier);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = aspnet_userID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@userID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();
            
            int userID = (int)command.Parameters["@userID"].Value;

            return userID;
        }

        public static DataSet getAllUsers()
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getAllUsers";
            command.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static DataSet getUserByID(int userID)
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getUserByID";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter userIDparam = new SqlParameter("@userID", SqlDbType.Int);
            userIDparam.Direction = ParameterDirection.Input;
            userIDparam.Value = userID;
            command.Parameters.Add(userIDparam);

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static bool addUser(string guidstring, string userLogin, string userEmail, string userFname, string userLname, int admin, out int userID, out string errorMessage)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            Guid aspnet_userID = new Guid(guidstring);

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_addUser";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@userLogin", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = userLogin;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@userEmail", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = userEmail;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@userFname", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = userFname;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@userLname", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = userLname;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@admin", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = admin;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@aspnet_userID", SqlDbType.UniqueIdentifier);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = aspnet_userID;
            command.Parameters.Add(parameter);
            
            parameter = new SqlParameter("@userID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@errorMessage", SqlDbType.NVarChar, 100);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            if (command.Parameters["@userID"].Value != DBNull.Value)
            {
                userID = Convert.ToInt32(command.Parameters["@userID"].Value);
            }
            else
            {
                userID = default(int);
            }
            errorMessage = (string)command.Parameters["@errorMessage"].Value;
            
            return succeeded;
        }

        public static bool updateUser(int userID, string userLogin, string userPass, string userEmail)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_updateUser";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@userID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = userID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@userLogin", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = userLogin;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@userPass", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = userPass;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@userEmail", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = userEmail;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            return succeeded;
        }

        public static bool deleteUser(int userID, out string errorMessage)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_deleteUser";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@userID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = userID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@errorMessage", SqlDbType.NVarChar, 100);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            errorMessage = (string)command.Parameters["@errorMessage"].Value;

            return succeeded;
        }

    }
}
