﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMSEverywhereLib
{
    public class Blogs
    {
        public static DataSet getBlogs()
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getBlogs";
            command.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static DataSet getBlogByID(int blogID)
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getBlogByID";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter blogIDparam = new SqlParameter("@blogID", SqlDbType.Int);
            blogIDparam.Direction = ParameterDirection.Input;
            blogIDparam.Value = blogID;
            command.Parameters.Add(blogIDparam);

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static bool addBlog(string blogName, out string errorMessage, out int blogID)
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_addBlog";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@blogName", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = blogName;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@errorMessage", SqlDbType.NVarChar, 100);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@blogID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            errorMessage = (string)command.Parameters["@errorMessage"].Value;
            blogID = (int)command.Parameters["@blogID"].Value;
            return succeeded;
        }

        public static bool updateBlog(string blogName, int blogID)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_updateBlog";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@blogName", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = blogName;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@blogID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = blogID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            return succeeded;
        }

        public static bool deleteBlog(int blogID, out string errorMessage)
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_deleteBlog";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@blogID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = blogID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@errorMessage", SqlDbType.NVarChar, 100);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            errorMessage = (string)command.Parameters["@errorMessage"].Value;

            return succeeded;
        }

    }
}
