﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMSEverywhereLib
{
    public class Images
    {
        public static DataSet getImages()
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getImages";
            command.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static DataSet getImagebyID(int imageID)
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getImagebyID";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@imageID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = imageID;
            command.Parameters.Add(parameter);

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static bool addImage(int imageAuthorID, string imagePath, string imageCaption, out int imageID, out string errorMessage)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_addImage";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@imageAuthorID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = imageAuthorID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@imagePath", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = imagePath;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@imageCaption", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = imageCaption;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@errorMessage", SqlDbType.NVarChar, 100);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@imageID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);
            imageID = (int)command.Parameters["@imageID"].Value;
            errorMessage = "";
            if (command.Parameters["@errorMessage"].Value != DBNull.Value) {
                errorMessage = (string)command.Parameters["@errorMessage"].Value;
            };

            return succeeded;
        }

        public static bool updateImage(int imageID, string imagePath, string imageCaption, out string errorMessage)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_updateImage";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@imageID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = imageID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@imagePath", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = imagePath;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@imageCaption", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = imageCaption;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@errorMessage", SqlDbType.NVarChar, 100);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            errorMessage = (string)command.Parameters["@errorMessage"].Value;

            return succeeded;
        }

        public static bool deleteImage(int imageID, out string errorMessage)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_deleteImage";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@imageID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = imageID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@errorMessage", SqlDbType.NVarChar, 100);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            errorMessage = (string)command.Parameters["@errorMessage"].Value;

            return succeeded;
        }
    }
}
