﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;

namespace CMSEverywhereLib
{
    class DataServices
    {
        public SqlConnection SetDatabaseConnection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["dbConnect"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            return connection;
        }
    }
}
