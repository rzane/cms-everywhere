﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMSEverywhereLib
{
    public class PostHistory
    {
        public static DataSet getPostHistoryTitleByPostID(int postID)
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getPostHistoryTitleByPostID";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter postIDparam = new SqlParameter("@postID", SqlDbType.Int);
            postIDparam.Direction = ParameterDirection.Input;
            postIDparam.Value = postID;
            command.Parameters.Add(postIDparam);

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static DataSet getPostHistoryByPostHistID(int postHistoryID)
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getPostHistoryByPostHistID";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter postHistoryIDparam = new SqlParameter("@postHistoryID", SqlDbType.Int);
            postHistoryIDparam.Direction = ParameterDirection.Input;
            postHistoryIDparam.Value = postHistoryID;
            command.Parameters.Add(postHistoryIDparam);

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static bool addPostHistory(int postID, int oldpostAuthorID, string oldpostContent, string oldpostTitle, out string errorMessage)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_addPostReadHistory";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@postID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = postID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@oldpostAuthorID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = oldpostAuthorID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@oldpostContent", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = oldpostContent;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@oldpostTitle", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = oldpostTitle;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@errorMessage", SqlDbType.NVarChar, 100);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            errorMessage = (string)command.Parameters["@errorMessage"].Value;

            return succeeded;
        }

    }
}
