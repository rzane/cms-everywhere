﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMSEverywhereLib
{
    public class Posts
    {
        public static DataSet getPosts()
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getPosts";
            command.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static DataSet getPostsByBlog(int blogID)
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getPostsByBlog";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter blogIDparam = new SqlParameter("@blogID", SqlDbType.Int);
            blogIDparam.Direction = ParameterDirection.Input;
            blogIDparam.Value = blogID;
            command.Parameters.Add(blogIDparam);

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static DataSet getPostbyID(int postID, int readerORauthor)
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getPostbyID";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter postIDparam = new SqlParameter("@postID", SqlDbType.Int);
            postIDparam.Direction = ParameterDirection.Input;
            postIDparam.Value = postID;
            command.Parameters.Add(postIDparam);

            SqlParameter readerORauthorparam = new SqlParameter("@readerORauthor", SqlDbType.Int);
            readerORauthorparam.Direction = ParameterDirection.Input;
            readerORauthorparam.Value = readerORauthor;
            command.Parameters.Add(readerORauthorparam);


            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static bool addPost(int postAuthorID, string postContent, string postTitle, out string errorMessage, out int postID)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_addPost";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@postAuthorID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = postAuthorID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@postContent", SqlDbType.NVarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = postContent;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@postTitle", SqlDbType.NVarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = postTitle;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@errorMessage", SqlDbType.NVarChar, 100);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@postID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            errorMessage = (string)command.Parameters["@errorMessage"].Value;

            postID = (int)command.Parameters["@postID"].Value;

            return succeeded;
        }

        public static bool updatePost(int postID, int postAuthorID, string postContent, string postTitle, out string errorMessage)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_updatePost";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@postID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = postID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@postAuthorID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = postAuthorID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@postContent", SqlDbType.NVarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = postContent;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@postTitle", SqlDbType.NVarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = postTitle;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@errorMessage", SqlDbType.NVarChar, 2000);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);
            
            parameter.Value = command.Parameters["@errorMessage"].Value;

            if (parameter.Value == null || parameter.Value == DBNull.Value)
            {
                errorMessage = "";
            }
            else
            {
                errorMessage = (string)command.Parameters["@errorMessage"].Value;
            }

            return succeeded;
        }

        public static bool addPostToBlog(int postID, int blogID, out string errorMessage)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_addPostToBlog";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@postID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = postID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@blogID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = blogID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@errorMessage", SqlDbType.NVarChar, 100);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);
            var result = command.Parameters["@errorMessage"].Value;
            errorMessage = (result == DBNull.Value) ? string.Empty : result.ToString();
            return succeeded;
        }

        public static bool deletePost(int postID, out string errorMessage)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_deletePost";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@postID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = postID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@errorMessage", SqlDbType.NVarChar, 100);
            parameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            errorMessage = (string)command.Parameters["@errorMessage"].Value;

            return succeeded;
        }

    }
}
