﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMSEverywhereLib
{
    public class Layouts
    {
        public static DataSet getLayouts()
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getLayouts";
            command.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static DataSet getLayoutByID(int layoutID)
        {
            DataSet data = new DataSet();
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_getLayoutByID";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter layoutIDparam = new SqlParameter("@layoutID", SqlDbType.Int);
            layoutIDparam.Direction = ParameterDirection.Input;
            layoutIDparam.Value = layoutID;
            command.Parameters.Add(layoutIDparam);

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(data);
            connection.Close();
            return data;
        }

        public static bool updateLayout(int layoutID, string layout)
        {
            DataServices dataServices = new DataServices();
            SqlConnection connection = dataServices.SetDatabaseConnection();

            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "proc_updateLayout";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter = new SqlParameter("@layoutID", SqlDbType.Int);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = layoutID;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@layout", SqlDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = layout;
            command.Parameters.Add(parameter);

            parameter = new SqlParameter("@succeeded", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.ReturnValue;
            command.Parameters.Add(parameter);

            command.ExecuteNonQuery();

            bool succeeded = Convert.ToBoolean(command.Parameters["@succeeded"].Value);

            return succeeded;
        }

    }
}
