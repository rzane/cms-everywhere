﻿using System;
using CMSEverywhereLib;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CMSEverywhereUnitTest
{
    [TestClass]
    public class PostsUnitTest
    {
        [TestMethod]
        public void getPostsTest()
        {
            int expectedNumberOfPosts = 9;
            int actualNumberOfPosts = 0;
            DataSet dataSet = Posts.getPosts();
            actualNumberOfPosts = dataSet.Tables[0].Rows.Count;

            Assert.AreEqual(expectedNumberOfPosts, actualNumberOfPosts, "Expected number of posts differs from actual");
        }
        [TestMethod]
        public void getPostsByBlogTest()
        {
            int expectedNumberOfPosts = 4;
            int actualNumberOfPosts;

            int blogID = 1;
            DataSet dataSet = Posts.getPostsByBlog(blogID);
            actualNumberOfPosts = dataSet.Tables[0].Rows.Count;

            Assert.AreEqual(expectedNumberOfPosts, actualNumberOfPosts, "Expected number of posts differs from actual");
        }
        [TestMethod]
        public void getPostbyID()
        {
            int expectedNumberOfPosts = 1;
            int actualNumberOfPosts;

            int postID = 1;
            DataSet dataSet = Posts.getPostbyID(postID, 1);
            actualNumberOfPosts = dataSet.Tables[0].Rows.Count;

            Assert.AreEqual(expectedNumberOfPosts, actualNumberOfPosts, "Expected number of posts differs from actual");
        }
        [TestMethod]
        public void updatePost()
        {
            int postID = 1;
            int postAuthorID = 1;
            string postContent = "Updated Post";
            string postTitle = "Updated Title";
            string errorMessage;

            bool succeeded = Posts.updatePost(postID, postAuthorID, postContent, postTitle, out errorMessage);
            Assert.IsTrue(succeeded, "Update Post Failed");
        }
        [TestMethod]
        public void addPostTest()
        {
            int postAuthorID = 1;
            string postContent = "New Post Content";
            string postTitle = "New Post Title";
            int postID;
            string errorMessage;

            bool succeeded = Posts.addPost(postAuthorID, postContent, postTitle, out errorMessage, out postID);
            Assert.IsTrue(succeeded, "Add Post Failed");

            bool delsucceeded = Posts.deletePost(postID, out errorMessage);

            Assert.IsTrue(delsucceeded, "Expected post delete to succeed");
        }
    }
}
