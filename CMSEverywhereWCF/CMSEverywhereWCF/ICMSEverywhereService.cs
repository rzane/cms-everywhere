﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace CMSEverywhereWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ICMSEverywhereService
    {

        [OperationContract]
        int GetUserID(string aspnet_userID);

        [OperationContract]
        DataSet getImages();

        [OperationContract]
        DataSet getImagebyID(int imageID);

        [OperationContract]
        bool addImage(int imageAuthorID, string imagePath, string imageCaption, out int imageID, out string errorMessage);

        [OperationContract]
        bool updateImage(int imageID, string imagePath, string imageCaption, out string errorMessage);

        [OperationContract]
        bool deleteImage(int imageID, out string errorMessage);

        [OperationContract]
        DataSet getPosts();

        [OperationContract]
        DataSet getPostsByBlog(int blogID);

        [OperationContract]
        DataSet getPostbyID(int postID, int readerORauthor);

        [OperationContract]
        bool addPost(int postAuthorID, string postContent, string postTitle, out string errorMessage, out int postID);

        [OperationContract]
        bool updatePost(int postID, int postAuthorID, string postContent, string postTitle, out string errorMessage);

        [OperationContract]
        bool addPostToBlog(int postID, int blogID, out string errorMessage);

        [OperationContract]
        bool deletePost(int postID, out string errorMessage);

        [OperationContract]
        DataSet getBlogs();

        [OperationContract]
        DataSet getBlogByID(int blogID);

        [OperationContract]
        bool addBlog(string blogName, out string errorMessage, out int blogID);

        [OperationContract]
        bool updateBlog(string blogName, int blogID);

        [OperationContract]
        bool deleteBlog(int blogID, out string errorMessage);

        [OperationContract]
        DataSet getAllGA();

        [OperationContract]
        DataSet getGAbyID(int gaID);

        [OperationContract]
        bool addGALoginInfo(string gaLogin, string gaPass, out string errorMessage);

        [OperationContract]
        bool updateGALoginInfo(int gaID, string gaLogin, string gaPass);

        [OperationContract]
        bool deleteGALoginInfo(int gaID, out string errorMessage);

        [OperationContract]
        DataSet getAllUsers();

        [OperationContract]
        DataSet getUserByID(int userID);

        [OperationContract]
        bool addUser(string guidstring, string userLogin, string userEmail, string userFname, string userLname, int admin, out int userID, out string errorMessage);

        [OperationContract]
        bool updateUser(int userID, string userLogin, string userPass, string userEmail);

        [OperationContract]
        bool deleteUser(int userID, out string errorMessage);

        [OperationContract]
        DataSet getLayouts();

        [OperationContract]
        DataSet getLayoutByID(int layoutID);

        [OperationContract]
        bool updateLayout(int layoutID, string layout);

        [OperationContract]
        DataSet getPostHistoryTitleByPostID(int postID);

        [OperationContract]
        DataSet getPostHistoryByPostHistID(int postHistoryID);

        [OperationContract]
        bool addPostHistory(int postID, int oldpostAuthorID, string oldpostContent, string oldpostTitle, out string errorMessage);

        [OperationContract]
        bool addPostReadHistory(int postID, out string errorMessage);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
