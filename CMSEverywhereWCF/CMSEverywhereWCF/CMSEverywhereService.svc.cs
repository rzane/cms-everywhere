﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using CMSEverywhereLib;

namespace CMSEverywhereWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class CMSEverywhereService : ICMSEverywhereService
    {
        public DataSet getImages()
        {
          return Images.getImages();
        }

        public DataSet getImagebyID(int imageID)
        {
          return Images.getImagebyID(imageID);
        }

        public int GetUserID(string aspnet_userID)
        {
            return Users.GetUserID(aspnet_userID);
        }
        
        public bool addImage(int imageAuthorID, string imagePath, string imageCaption, out int imageID, out string errorMessage)
        {
          return Images.addImage(imageAuthorID, imagePath, imageCaption, out imageID, out errorMessage);
        }

        public bool updateImage(int imageID, string imagePath, string imageCaption, out string errorMessage)
        {
          return Images.updateImage(imageID, imagePath, imageCaption, out errorMessage);
        }

        public bool deleteImage(int imageID, out string errorMessage)
        {
          return Images.deleteImage(imageID, out errorMessage);
        }

        public DataSet getPosts()
        {
          return Posts.getPosts();
        }

        public DataSet getPostsByBlog(int blogID)
        {
          return Posts.getPostsByBlog(blogID);
        }

        public DataSet getPostbyID(int postID, int readerORauthor)
        {
          return Posts.getPostbyID(postID, readerORauthor);
        }

        public bool addPost(int postAuthorID, string postContent, string postTitle, out string errorMessage, out int postID)
        {
          return Posts.addPost(postAuthorID, postContent, postTitle, out errorMessage, out postID);
        }

        public bool updatePost(int postID, int postAuthorID, string postContent, string postTitle, out string errorMessage)
        {
          return Posts.updatePost(postID, postAuthorID, postContent, postTitle, out errorMessage);
        }

        public bool addPostToBlog(int postID, int blogID, out string errorMessage)
        {
          return Posts.addPostToBlog(postID, blogID, out errorMessage);
        }

        public bool deletePost(int postID, out string errorMessage)
        {
          return Posts.deletePost(postID, out errorMessage);
        }

        public DataSet getBlogs()
        {
          return Blogs.getBlogs();
        }

        public DataSet getBlogByID(int blogID)
        {
          return Blogs.getBlogByID(blogID);
        }

        public bool addBlog(string blogName, out string errorMessage, out int blogID)
        {
          return Blogs.addBlog(blogName, out errorMessage, out blogID);
        }

        public bool updateBlog(string blogName, int blogID)
        {
          return Blogs.updateBlog(blogName, blogID);
        }

        public bool deleteBlog(int blogID, out string errorMessage)
        {
          return Blogs.deleteBlog(blogID, out errorMessage);
        }

        public DataSet getAllGA()
        {
          return GoogleAnalytics.getAllGA();
        }

        public DataSet getGAbyID(int gaID)
        {
          return GoogleAnalytics.getGAbyID(gaID);
        }

        public bool addGALoginInfo(string gaLogin, string gaPass, out string errorMessage)
        {
          return GoogleAnalytics.addGALoginInfo(gaLogin, gaPass, out errorMessage);
        }

        public bool updateGALoginInfo(int gaID, string gaLogin, string gaPass)
        {
          return GoogleAnalytics.updateGALoginInfo(gaID, gaLogin, gaPass);
        }

        public bool deleteGALoginInfo(int gaID, out string errorMessage)
        {
          return GoogleAnalytics.deleteGALoginInfo(gaID, out errorMessage);
        }

        public DataSet getAllUsers()
        {
          return Users.getAllUsers();
        }

        public DataSet getUserByID(int userID)
        {
          return Users.getUserByID(userID);
        }

        public bool addUser(string guidstring, string userLogin, string userEmail, string userFname, string userLname, int admin, out int userID, out string errorMessage)
        {
          return Users.addUser(guidstring, userLogin, userEmail, userFname, userLname, admin, out userID, out errorMessage);
        }

        public bool updateUser(int userID, string userLogin, string userPass, string userEmail)
        {
          return Users.updateUser(userID, userLogin, userPass, userEmail);
        }

        public bool deleteUser(int userID, out string errorMessage)
        {
          return Users.deleteUser(userID, out errorMessage);
        }

        public DataSet getLayouts()
        {
          return Layouts.getLayouts();
        }

        public DataSet getLayoutByID(int layoutID)
        {
          return Layouts.getLayoutByID(layoutID);
        }

        public bool updateLayout(int layoutID, string layout)
        {
          return Layouts.updateLayout(layoutID, layout);
        }

        public DataSet getPostHistoryTitleByPostID(int postID)
        {
          return PostHistory.getPostHistoryTitleByPostID(postID);
        }

        public DataSet getPostHistoryByPostHistID(int postHistoryID)
        {
          return PostHistory.getPostHistoryByPostHistID(postHistoryID);
        }

        public bool addPostHistory(int postID, int oldpostAuthorID, string oldpostContent, string oldpostTitle, out string errorMessage)
        {
          return PostHistory.addPostHistory(postID, oldpostAuthorID, oldpostContent, oldpostTitle, out errorMessage);
        }

        public bool addPostReadHistory(int postID, out string errorMessage)
        {
          return PostReadHistory.addPostReadHistory(postID, out errorMessage);
        }
    }
}
