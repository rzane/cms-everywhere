﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSEverywhereWeb.User
{
    public partial class ImageShow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int imgID = Convert.ToInt32(Request.QueryString["imgID"]);
            CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient client = new CMSEverywhereService.CMSEverywhereServiceClient();
            DataSet ds = client.getImagebyID(imgID);
            string imgpath = ds.Tables[0].Rows[0]["imagePath"].ToString();
            string imageCaption = ds.Tables[0].Rows[0]["imageCaption"].ToString();
            string imageID = ds.Tables[0].Rows[0]["imageID"].ToString();
            string authorname = ds.Tables[0].Rows[0]["userLogin"].ToString();

            LiteralControl lc = new LiteralControl("<img id='image' src=\"" + "/img/testimg.jpeg"/*imgpath*/ + "\" />");
            imageholder.Controls.Add(lc);
            imagecaptionedit.Text = imageCaption;
            caption.InnerText = imageCaption;

            author.InnerText = authorname;

            filename.Text = imgpath;
        }
    }
}