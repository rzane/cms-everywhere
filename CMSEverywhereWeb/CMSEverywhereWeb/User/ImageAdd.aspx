﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"  CodeBehind="ImageAdd.aspx.cs" Inherits="CMSEverywhereWeb.User.ImageAdd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="well">
        <h3>Add Image</h3>
        <p><asp:FileUpload id="FileUploadControl" runat="server" /></p>
        <p><asp:TextBox ID="caption" placeholder="Caption" runat="server" /></p>
        <p><asp:Button runat="server" id="UploadButton" text="Upload" onclick="UploadButton_Click" />
        </p>
        <br /><br />
        <asp:Label runat="server" id="StatusLabel" text="Upload status: " />
    </div>
</asp:Content>