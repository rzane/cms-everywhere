﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="PostHistShow.aspx.cs" Inherits="CMSEverywhereWeb.User.PostHistShow" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main_content">
        <div id="posthistshow" class="well">
            <h3 id="posttitle" runat="server"></h3>
            <blockquote>
                <p id="postcontent" runat="server"></p>
                <small><span id="postauthor" runat="server"></span> on <span id="postdate" runat="server"></span></small>
            </blockquote>
        </div>
    </div>
</asp:Content>
