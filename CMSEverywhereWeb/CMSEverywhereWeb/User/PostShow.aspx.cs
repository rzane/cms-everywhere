﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSEverywhereWeb.User
{
    public partial class PostShow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int postID = Convert.ToInt32(Request.QueryString["postID"]);
            CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient client = new CMSEverywhereService.CMSEverywhereServiceClient();
            populatePostInfo(postID, client);
            populatePostHistInfo(postID, client);
        }

        public void populatePostInfo(int postID, CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient client)
        {
            DataSet ds = client.getPostbyID(postID, 0);
            string posttitle = Convert.ToString(ds.Tables[0].Rows[0]["postContent"]);
            string postcontent = Convert.ToString(ds.Tables[0].Rows[0]["postTitle"]);
            string author = Convert.ToString(ds.Tables[0].Rows[0]["userLogin"]);
            string date = Convert.ToString(ds.Tables[0].Rows[0]["postDate"]);
            int id = Convert.ToInt32(ds.Tables[0].Rows[0]["postID"]);

            update.Attributes.Add("data-id", id.ToString());

            content.Text = posttitle;
            showcontent.InnerText = posttitle;
            title.Text = postcontent;
            showtitle.InnerText = postcontent;
            showauthor.InnerText = author;
            showpostdate.InnerText = date;
        }

        public void populatePostHistInfo(int postID, CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient client)
        {
            DataSet ds = client.getPostHistoryTitleByPostID(postID);
            ddlPostHist.DataSource = ds;
            ddlPostHist.DataTextField = "titletext";
            ddlPostHist.DataValueField = "PostHistoryID";
            ddlPostHist.DataBind();

        }
    }
}