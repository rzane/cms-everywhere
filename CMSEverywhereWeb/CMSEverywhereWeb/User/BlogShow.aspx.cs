﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSEverywhereWeb.User
{
    public partial class BlogShow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int blogID = Convert.ToInt32(Request.QueryString["blogID"]);
            CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient client = new CMSEverywhereService.CMSEverywhereServiceClient();
            DataSet ds = client.getBlogByID(blogID);
            string blogname = Convert.ToString(ds.Tables[0].Rows[0]["blogName"]);
            editblogname.Attributes.Add("data-id",blogID.ToString());
            editblogname.Text = blogname;
            showblogname.InnerText = blogname;


            ds = client.getPostsByBlog(blogID);
            postsbyblog.DataSource = ds;
            postsbyblog.DataBind();

            postsbyblog.UseAccessibleHeader = true;

            commitpoststoblog.Attributes.Add("data-id", blogID.ToString());
        }
    }
}