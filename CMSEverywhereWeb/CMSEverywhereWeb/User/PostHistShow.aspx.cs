﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSEverywhereWeb.User
{
    public partial class PostHistShow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int posthistid = Convert.ToInt32(Request.QueryString["postHistID"]);
            CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient client = new CMSEverywhereService.CMSEverywhereServiceClient();
            DataSet ds = client.getPostHistoryByPostHistID(posthistid);

            string title = Convert.ToString(ds.Tables[0].Rows[0]["postContent"]);
            string content = Convert.ToString(ds.Tables[0].Rows[0]["postTitle"]);
            string author = Convert.ToString(ds.Tables[0].Rows[0]["userLogin"]);
            string date = Convert.ToString(ds.Tables[0].Rows[0]["postDate"]);

            postcontent.InnerText = title;
            posttitle.InnerText = content;
            postauthor.InnerText = author;
            postdate.InnerText = date;
        }
    }
}