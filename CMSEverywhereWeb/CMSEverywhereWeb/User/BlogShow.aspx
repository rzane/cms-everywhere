﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="BlogShow.aspx.cs" Inherits="CMSEverywhereWeb.User.BlogShow" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main_content">
        <div class="well blogshow">
            <div>
                <button class="btn btn-primary showbutton" id="backbutton" type="button">Back</button>
                <button type="button" id="editmode" class="btn showbutton">Edit Blog</button>
                <button type="button" id="update" class="btn btn-primary editbutton" style="display: none;">Update</button>
                <button type="button" id="showmode" class="btn editbutton" style="display: none;">Cancel</button>
                <button type="button" id="delete" class="btn pull-right editbutton" style="display: none;">Delete</button>
            </div><br />
            <div id="show">
                <div>
                    <h1 id="showblogname" runat="server"></h1>
                </div>
            </div>

            <div id="edit" class="hide">
                <p>
                    <b>Blog Name:</b><br />
                    <asp:TextBox runat="server" CssClass="span5" ID="editblogname"></asp:TextBox>
                </p>
            </div>
        </div>
        <div class="well blogshowpostlist">
            <button type="button" id="addpoststoblog" class="btn pull-right" style="margin-bottom:20px">Add Posts</button>
            <asp:GridView runat="server" id="postsbyblog" AutoGenerateColumns="false" CssClass="postbyblog hovertable table table-striped" GridLines="None">
                <Columns>
                    <asp:BoundField DataField="postID" HeaderText="#" />
                    <asp:BoundField HeaderText="Title" DataField="postTitle"/>
                    <asp:BoundField HeaderText="Author" DataField="userLogin" />
                    <asp:BoundField HeaderText="Date" DataField="postDate"/>
                </Columns>
            </asp:GridView>
        </div>

        <div class="well addblogblogshow" style="display:none;">
                <h1>Add Posts to Blog</h1>
                <p style="margin-bottom:30px;">
                    <select id="multiselect" multiple="multiple">
                    </select>
                    <button type="button" id="commitpoststoblog" runat="server" class="btn btn-primary hide">Add</button>
                </p>
                <p>
                    <button type="button" id="addposttoblogback" class="btn btn-primary">Back</button>
                </p>
        </div>
    </div>
</asp:Content>

