﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Images.aspx.cs" Inherits="CMSEverywhereWeb.User.Images" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        #imagesindex .imagecontainer {
            text-align:center;
        }
        #imagesindex .imagecontainer:hover {
            background-color: #0094ff;
            color: #ffffff;
        }
        #imageshow .imagecontainer {
            text-align: center;
        }
        
    </style>

    <div id="main_content">
        <div id="imagesindex">
            <div id="imagescontainer" runat="server"></div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            prepImagesIndex();
        });

        var imagesindexcontent;

        function prepImagesIndex() {
            $('.imagecontainer').unbind('click').on('click', function () {
                var id = $(this).closest('.imagecontainer').data('imgid');
                var url = "ImageShow.aspx?imgID=" + id + " #main_content > *";
                $('div#loading').show();
                imagesindexcontent = $('#main_content > *');
                $('#main_content').empty().load(url, function () {
                    prepImageShow();
                    $('div#loading').hide();
                });

            });
        }

        function prepImageShow() {
            $('#imageshow #editmode').unbind('click').on('click', function () {
                $('.show').hide();
                $('.edit').show();
            });
            $('#imageshow #showmode').unbind('click').on('click', function () {
                $('.edit').hide();
                $('.show').show();
            });
            $('#imageshow #backbutton').unbind('click').on('click', function () {
                $('#main_content').empty().html(imagesindexcontent);
                prepImagesIndex();
            });
        }
    </script>
</asp:Content>
