﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Blogs.aspx.cs" Inherits="CMSEverywhereWeb.User.Blogs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="/js/blogs.js" type="text/javascript"></script>
    <div id="main_content">
        <div class="well blogs">
            <div id="addbuttonholder">
                <button id='addblog' style='margin-bottom: 15px;' type='button' class='btn btn-primary pull-right'>Add Blog</button>
            </div>
        <asp:GridView runat="server" id="bloglist" DataSourceID="odsBlogs" AutoGenerateColumns="false" CssClass="hovertable bloglist table table-striped" GridLines="None">
            <Columns>
                <asp:BoundField DataField="blogID" HeaderText="#" />
                <asp:BoundField DataField="blogName" HeaderText="Name" />
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="odsBlogs" runat="server" TypeName="CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient" SelectMethod="getBlogs"/>
            </div>
    </div>
    <script type="text/javascript" src="/js/bootstrap-multiselect.js"></script>
</asp:Content>
