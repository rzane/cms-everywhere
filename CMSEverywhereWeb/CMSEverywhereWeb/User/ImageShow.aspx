﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"  CodeBehind="ImageShow.aspx.cs" Inherits="CMSEverywhereWeb.User.ImageShow" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="main_content">
        <div id="imageshow">
        <div class="well imagecontainer">
            <button class="show btn btn-primary" id="backbutton" type="button">Back</button><br />
            <div runat="server" id="imageholder"></div>
        </div>
        <div class="well">
            <div id="show" class="show">
                <b>Author: </b>
                <div runat="server" id="author"></div>
                <br />
                <b>Caption: </b>
                <div runat="server" id="caption"></div>
            </div>
            <div id="edit" class="edit hide">
                <p>
                    <b>Image: </b><br />
                    <asp:TextBox runat="server" ID="filename" /><button style="margin-bottom: 10px;" class="btn" type="button">Replace</button>
                </p>
                <p>
                    <b>Caption: </b><br />
                    <asp:TextBox runat="server" TextMode="MultiLine" ID="imagecaptionedit" />
                </p>
            </div><br />
            <div id="postbuttons">
                <button type="button" id="editmode" class="show btn">Edit Image</button>
                <button type="button" id="showmode" class="edit btn" style="display: none;">Cancel</button>
                <button type="button" id="update" class="edit btn btn-primary" style="display: none;">Update</button>
            </div>

        </div>
    </div>
    </div>
    <style>
        #imageshow .imagecontainer {
            text-align: center;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('#editmode').on('click', function () {
                $('.show').hide();
                $('.edit').show();
            });
            $('#showmode').on('click', function () {
                $('.edit').hide();
                $('.show').show();
            });
        });

    </script>
</asp:Content>
