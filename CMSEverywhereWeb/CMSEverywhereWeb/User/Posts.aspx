﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Posts.aspx.cs" Inherits="CMSEverywhereWeb.User.AddPost" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="/js/posts.js" type="text/javascript"></script>
    <div id="main_content">
        <div class="well postsdiv">
            <div id="addbuttonholder">
                <button id='addpost' style='margin-bottom: 15px;' type='button' class='btn btn-primary pull-right'>Add Post</button>
            </div>
        <asp:GridView runat="server" id="postlist" DataSourceID="odsPosts" AutoGenerateColumns="false" CssClass="postlist hovertable table table-striped" GridLines="None">
            <Columns>
                <asp:BoundField DataField="postID" HeaderText="#" />
                <asp:BoundField HeaderText="Title" DataField="postTitle"/>
                <asp:BoundField HeaderText="Author" DataField="userLogin" />
                <asp:BoundField HeaderText="Date" DataField="postDate"/>
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="odsPosts" runat="server" TypeName="CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient" SelectMethod="getPosts"/>
        </div>
        <div class="well addpostdiv" style="display:none;">
            <h1>New Post</h1>
            <p>
                <input type="text" id="newposttitle" placeholder="Title" />
            </p>
            <p>
                <textarea id="newpostcontent" placeholder="Content"></textarea>
            </p>
            <p>
                <button type="button" id="addpostbackbutton" class="btn">Back</button>
                <button type="button" id="newpostcommit" runat="server" class="btn btn-primary">Add</button>
            </p>
        </div>
    </div>
</asp:Content>