﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSEverywhereWeb.User
{
    public partial class Images : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient client = new CMSEverywhereService.CMSEverywhereServiceClient();
            DataSet ds = client.getImages();
            DataTable dt = ds.Tables[0];
            StringBuilder sb = new StringBuilder();

            foreach (DataRow row in dt.Rows)
            {
                sb.Append("<div class='well span2 imagecontainer' data-imgid=\"" + row["imageID"].ToString() + "\" >");
                sb.Append("<div class='imageholder'>");
                sb.Append("<img src=\"" + "/img/testimg.jpeg"/*row["imagePath"].ToString()*/ + "\" />");
                sb.Append("</div>");
                sb.Append("<div class='textholder'");
                sb.Append("<p class='captionholder'>" + row["imageCaption"] + "</p>");
                sb.Append("<span class='authorholder'><i>" + row["userLogin"] + "</i></span>");
                sb.Append("</div>");
                sb.Append("</div>");
            }

            LiteralControl lc = new LiteralControl(sb.ToString());
            imagescontainer.Controls.Add(lc);
        }
    }
}