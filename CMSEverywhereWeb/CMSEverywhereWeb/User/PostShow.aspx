﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="PostShow.aspx.cs" Inherits="CMSEverywhereWeb.User.PostShow" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main_content">
        <div class="well postshow">
            <div id="show">
                <div>
                    <h3 id="showtitle" runat="server"></h3>
                </div>
                <br />
                <div>
                    <blockquote>
                        <p id="showcontent" runat="server"></p>
                        <small><span id="showauthor" runat="server"></span> on <span id="showpostdate" runat="server"></span></small>
                    </blockquote>
                </div>
            </div>

            <div id="edit" class="hide">
                <p>
                    <b>Post Title:</b><br />
                    <asp:TextBox runat="server" CssClass="span5" ID="title"></asp:TextBox>
                </p>
                <p>
                    <b>Post Content:</b><br />
                    <asp:TextBox runat="server" CssClass="span5" ID="content" TextMode="multiline"></asp:TextBox>
                </p>
            </div>
            <br />
            <br />
            <div id="postbuttons">
                <button class="btn btn-primary" id="backbutton" type="button">Back</button>
                <button type="button" id="editmode" class="btn" data-authorid="<%= Session["userID"] %>">Edit Post</button>
                <button type="button" id="showmode" class="btn" style="display: none;">Cancel</button>
                <button type="button" id="update" runat="server" class="btn btn-primary" style="display: none;">Update</button>
                <button type="button" id="delete" class="btn pull-right" style="display: none;">Delete</button>
            </div>
        </div>
        <div id="posthistory" class="well">
            <h3>Post History</h3>
            <div id="posthistorytitles">
                <asp:DropDownList runat="server" CssClass="span5" ID="ddlPostHist" /><button type="button" class="btn" id="posthistbutton" style="margin-bottom:10px;margin-left:10px;">View</button>
            </div>
            <div id="posthistorycontent"></div>
        </div>
    </div>
        <script>
            $(document).ready(function () {
                prepEditShowMode();
            });

            function prepEditShowMode() {
                $('#editmode').on('click', function () {
                    $('#show').hide();
                    $('#edit').show();
                    $('#editmode').hide();
                    $('#showmode').show();
                    $('#update').show();
                });
                $('#showmode').on('click', function () {
                    $('#show').show();
                    $('#edit').hide();
                    $('#showmode').hide();
                    $('#editmode').show();
                    $('#update').hide();
                });
            }
    </script>
</asp:Content>
