﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace CMSEverywhereWeb
{
    public class BlogsController : ApiController
    {
        private static CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient client = new CMSEverywhereService.CMSEverywhereServiceClient();

        [HttpPost]
        [ActionName("AddPosts")]
        //$.post('/api/Blogs/AddPosts/1', { '': "{postids:[1,2,3]}" }, function(data) {console.log(data)});
        public Dictionary<string, object> AddPosts(int id, [FromBody]string posts)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            Dictionary<string, object> dict = jss.Deserialize<Dictionary<string, object>>(posts);
            string errorMessage;
            Dictionary<string, object> responsedict = new Dictionary<string, object>();
            ArrayList created = new ArrayList();
            ArrayList failed = new ArrayList();
            foreach (string i in (ArrayList)dict["postids"])
            {
                int postID = Convert.ToInt32(i);
                bool successful = client.addPostToBlog(postID, id, out errorMessage);
                if (successful)
                {
                    created.Add(i);
                }
                else
                {
                    failed.Add(i);
                }
            }
            responsedict.Add("created", created);
            responsedict.Add("failed", failed);
            return responsedict;
        }



        // GET api/<controller>
        public DataTable Get()
        {
            DataSet ds = client.getBlogs();
            return ds.Tables[0];
        }

        // GET api/<controller>/5
        public Dictionary<string,object> Get(int id)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            DataSet ds = client.getBlogByID(id);
            string name = ds.Tables[0].Rows[0]["blogName"].ToString();
            dictionary.Add("id", id);
            dictionary.Add("name", name);
            return dictionary;
        }

        // POST api/<controller>
        public Dictionary<string,object> Post([FromBody]string blog)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            Dictionary<string, object> dict = jss.Deserialize<Dictionary<string, object>>(blog);
            string name = dict["name"].ToString();
            string errorMessage;
            int blogID;
            bool success = client.addBlog(name, out errorMessage, out blogID);
            dict.Add("success", success);
            if (success)
            {
                dict.Add("id", blogID);
            }
            else
            {
                dict.Add("error", errorMessage);
            }
            return dict;
        }

        // PUT api/<controller>/5
        public Dictionary<string,object> Put(int id, [FromBody]string blog)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            Dictionary<string, object> dict = jss.Deserialize<Dictionary<string, object>>(blog);
            string name = dict["name"].ToString();

            bool success = client.updateBlog(name, id);
            dict.Add("success", success);

            return dict;
        }

        // DELETE api/<controller>/5
        public Dictionary<string, object> Delete(int id)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string errorMessage;
            bool success = client.deleteBlog(id, out errorMessage);
            dict.Add("status", success);
            //success = false;
            
            return dict;
        }
    }
}