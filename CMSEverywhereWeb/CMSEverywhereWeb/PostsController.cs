﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace CMSEverywhereWeb
{
    public class PostsController : ApiController
    {
        private static CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient client = new CMSEverywhereService.CMSEverywhereServiceClient();

        // GET api/Posts
        public DataTable Get()
        {
            DataSet ds = client.getPosts();
            return ds.Tables[0];
        }

        // GET api/Posts/5
        public Array Get(int id)
        {
            DataSet ds = client.getPostbyID(id, 0);
            return ds.Tables[0].Rows[0].ItemArray;
        }

        // POST api/<controller>
        //$.post('api/Posts', { '': "value" }, function(data) {console.log(data)});
        //$.post('api/Posts', { '': "{id:'test'}" }, function(data) {console.log(data)});
        public Dictionary<string, object> Post([FromBody]string post)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            Dictionary<string, object> dict = jss.Deserialize<Dictionary<string, object>>(post);
            string title = dict["title"].ToString();
            string content = dict["content"].ToString();
            int authorid = Convert.ToInt32(dict["authorID"]);
            string errorMessage;
            int postID;
            bool success = client.addPost(authorid, content, title, out errorMessage, out postID);
            dict.Add("success", success);
            if (success)
            {
                dict.Add("id", postID);
            }
            else
            {
                dict.Add("error", errorMessage);
            }            
            return dict;
        }

        // PUT api/<controller>/5
        //$.ajax({
        //  url: 'api/Posts/1',
        //  type: 'PUT',
        //  data: {"":"{content:'test content', title: 'test title', authorID: 1}"},
        //  success: function(data) {
        //    console.log(data);
        //  }
        //});
        public Dictionary<string, object> Put(int id, [FromBody]string post)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            Dictionary<string, object> dict = jss.Deserialize<Dictionary<string, object>>(post);
            string title = dict["title"].ToString();
            string content = dict["content"].ToString();
            int authorid = Convert.ToInt32(dict["authorID"]);
            string errorMessage;
            bool success = client.updatePost(id, authorid, content, title, out errorMessage);
            dict.Add("success", success);
            if (!success)
            {
                dict.Add("error", errorMessage);
            }
            return dict;
        }

        // DELETE api/<controller>/5
        /*$.ajax({
            url: 'api/Posts/8',
            type: 'DELETE',
            success: function(result) {
        console.log(result);
            }
        });*/

        public Dictionary<string, object> Delete(int id)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string errorMessage;
            bool success = client.deletePost(id, out errorMessage);
            dict.Add("status", success);
            //success = false;

            return dict;
        }
    }
}