﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSEverywhereWeb
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void cmslogin_LoggedIn(object sender, EventArgs e)
        {

            string username = cmslogin.UserName;
            MembershipUser user = Membership.GetUser(username);

            int userID;
            Guid aspnet_userID;

            aspnet_userID = (Guid)user.ProviderUserKey;
            string aspnetuseridstring = Convert.ToString(aspnet_userID);
            CMSEverywhereService.CMSEverywhereServiceClient client =
                new CMSEverywhereService.CMSEverywhereServiceClient();
            userID = client.GetUserID(aspnetuseridstring);

            //Maintain data across all web pages / forms of the web app
            //Use session objects

            Session["userID"] = userID;
            Session["username"] = username;

            Response.Redirect("index.aspx");

        }
    }
}