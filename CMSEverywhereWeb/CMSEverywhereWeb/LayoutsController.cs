﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace CMSEverywhereWeb
{
    public class LayoutsController : ApiController
    {
        private static CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient client = new CMSEverywhereService.CMSEverywhereServiceClient();

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public Dictionary<string, object> Put(int id, [FromBody]string layout)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            Dictionary<string, object> dict = jss.Deserialize<Dictionary<string, object>>(layout);
            string newlayout = dict["layout"].ToString();

            bool success = client.updateLayout(id, newlayout);
            dict.Add("success", success);
            if (success)
            {
                dict.Add("id", id);
            }
            return dict;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}