﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Generate.aspx.cs" Inherits="CMSEverywhereWeb.Admin.Generate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main_content">
        <div class="well">
            <h1>Content Generator</h1>
            <p>Please choose which blog you would like to generate. Then input the ID of the DIV on your website that you would like to insert your blog into:</p>
            <dl>
                <dt>Blog: </dt>
                <dd>
                    <asp:DropDownList ID="ddlPatients" runat="server"
                    DataSourceID="odsBlogs"
                    DataTextField="blogName"
                    DataValueField="blogID"
                    AppendDataBoundItems="true">
                    </asp:DropDownList>
                </dd>
                <dt>DIV ID:</dt>
                <dd>
                    <input type="text" id="dividtxt"/>
                </dd>
            </dl>
            <button type="button" style="margin-bottom:10px;" id="generate" class="btn-primary btn">Generate</button>
            <button type="button" style="margin-bottom:10px;" id="demo" class="btn hide hidebeforegenclick">Demo</button>

            <asp:ObjectDataSource ID="odsBlogs" runat="server" TypeName="CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient" SelectMethod="getBlogs"/>
            <hr />
            <blockquote>
                <p id="contenturl"></p>
            </blockquote>
            <pre id="jscode" style="display:none;"></pre>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Demo</h3>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
      </div>
    </div>
    <script>
        $(document).ready(function () {
            prepareClicks();
        });

        function setURL() {
            var id = $('#ContentPlaceHolder1_ddlPatients').val();
            if (!window.location.origin) window.location.origin = window.location.protocol + "//" + window.location.host;
            var contenturl = window.location.origin + '/Admin/RenderBlog.aspx?blogID=' + id;
            var posturl = window.location.origin + '/Admin/RenderBlogPost.aspx?postID=';
            var divid = $('#dividtxt').val();
            $('#contenturl').text(contenturl);
            var js = "<html>\n\t<head>\n\t\t<script src='http://code.jquery.com/jquery-1.9.1.min.js'><\/script>\n\t\t";
            js += "<script>\
            \n\t\t\t$(function () {\
            \n\t\t\t\tloadIndex();\
            \n\t\t\t});\
            \n\t\t\tfunction loadIndex() {\
			\n\t\t\t\t$.get('"+ contenturl +"', function (data) {\
			\n\t\t\t\t\t$('#"+divid+"').empty().html(data);\
			\n\t\t\t\t\tprepClick();\
            \n\t\t\t\t});\
		    \n\t\t\t}\
            \n\t\t\tfunction prepClick() {\
            \n\t\t\t\t$('.postlink').click(function() {\
            \n\t\t\t\t\tvar id = $(this).data('id');\
            \n\t\t\t\t\tshowBlogPost(id);\
            \n\t\t\t\t});\
            \n\t\t\t}\
            \n\t\t\tfunction showBlogPost(id) {\
            \n\t\t\t\t$('#"+ divid +"').empty().load('"+ posturl +"' + id, function(data){\
            \n\t\t\t\t\t$('#back').click(function() { loadIndex(); });\
            \n\t\t\t\t});\
            \n\t\t\t}\
            \n\t\t<\/script>";
            js += "\n\t<\/head>";
            js += "\n\n\t<body>\n\t\t<div id='" + divid +"'><\/div>\n\t<\/body>\n<\/html>";
            $('#jscode').show().text(js);
        }

        function prepareClicks() {
            $(document).on('click', 'button#generate', function () {
                console.log('click');
                $('.hidebeforegenclick').show();
                setURL();
            });

            $(document).on('click', 'button#demo', function () {
                console.log('demo click');
                var id = $('#ContentPlaceHolder1_ddlPatients').val();
                genDemo(id);
            });
        }

        function genDemo(id) {
            $('div#loading').show();
            $('div.modal-body').load('/Admin/RenderBlog.aspx?blogID=' + id, function () { $('div#loading').hide(); });
            $('#myModal').modal('show');
        }
    </script>
</asp:Content>