﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSEverywhereWeb.Admin
{
    public partial class RenderBlogPost : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int postID = Convert.ToInt32(Request.QueryString["postID"]);
            CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient client = new CMSEverywhereService.CMSEverywhereServiceClient();
            DataSet dsLayouts = client.getLayoutByID(2);
            string blogpostlayout = dsLayouts.Tables[0].Rows[0]["layout"].ToString();
            DataSet dsPost = client.getPostbyID(postID, 1);

            string bloghtmlstring = generatePost(blogpostlayout, dsPost);

            LiteralControl lc = new LiteralControl(bloghtmlstring);
            content.Controls.Add(lc);
        }

        public string generatePost(string blogpostlayout, DataSet dsPost)
        {
            StringBuilder sb = new StringBuilder();
            string replacedhtml = blogpostlayout.Replace("#{TITLE}", dsPost.Tables[0].Rows[0]["postTitle"].ToString())
            .Replace("#{CONTENT}", dsPost.Tables[0].Rows[0]["postContent"].ToString())
            .Replace("#{AUTHOR}", dsPost.Tables[0].Rows[0]["userLogin"].ToString())
            .Replace("#{DATE}", dsPost.Tables[0].Rows[0]["postDate"].ToString())
            .Replace("#{BACK}", "<a href='#' id='back'>Back</a>");
            sb.Append(replacedhtml);
            return sb.ToString();
        }
    }
}