﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSEverywhereWeb
{
    public partial class RenderBlog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int blogID = Convert.ToInt32(Request.QueryString["blogID"]);
            CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient client = new CMSEverywhereService.CMSEverywhereServiceClient();
            DataSet dsLayouts = client.getLayoutByID(1);
            string blogindexlayout = dsLayouts.Tables[0].Rows[0]["layout"].ToString();
            DataSet dsBlogPosts = client.getPostsByBlog(blogID);
            DataTable dtBlogPosts = dsBlogPosts.Tables[0];
            
            string bloghtmlstring = generateBlog(blogindexlayout, dtBlogPosts);

            LiteralControl lc = new LiteralControl(bloghtmlstring);
            content.Controls.Add(lc);
        }
        
        public string generateBlog(string layout, DataTable blogposts)
        {
            StringBuilder sb = new StringBuilder();

            foreach (DataRow row in blogposts.Rows)
            {
                //sb.Append(row["postTitle"]);
                string replacedhtml = layout.Replace("#{TITLE}", row["postTitle"].ToString())
                    .Replace("#{CONTENT}", row["postContent"].ToString())
                    .Replace("#{AUTHOR}", row["userLogin"].ToString())
                    .Replace("#{DATE}", row["postDate"].ToString())
                    .Replace("#{PLSTART}", "<a href='#' class='postlink' data-id='" + row["postID"].ToString() + "'>")
                    .Replace("#{PLEND}", "</a>");
                sb.Append(replacedhtml);
            }

            return sb.ToString();
        }

    }
}