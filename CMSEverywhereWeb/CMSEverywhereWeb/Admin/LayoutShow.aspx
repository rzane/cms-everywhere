﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="LayoutShow.aspx.cs" Inherits="CMSEverywhereWeb.User.LayoutShow" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main_content">
        <script>alert('HELLO');</script>
        <div class="well">
            <h1 runat="server" id="layoutname"></h1>
            <div id="show">
                <pre runat="server" id="layouttext"></pre>
            </div>
            <div id="edit" class="hide">
                <textarea id="editor" class="span8" style="height:350px;"></textarea>
            </div>
            <div id="layoutbuttons" style="margin-top:15px;">
                <button type="button" class="btn btn-primary showmode" id="backbutton" >Back</button>
                <button type="button" class="btn showmode" id="editbutton" >Edit Layout</button>
                <button type="button" class="btn editmode hide" id="cancelbutton">Cancel</button>
                <button type="button" class="btn btn-primary editmode hide" runat="server" id="updatebutton">Update</button>
            </div>
        </div>

    </div>
</asp:Content>