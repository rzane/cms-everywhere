﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSEverywhereWeb.Admin
{
    public partial class UserAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
        {
            string firstname = tbFirstname.Text;
            string lastname = tbLastname.Text;
            string username = CreateUserWizard1.UserName;
            string userEmail = CreateUserWizard1.Email;

            int admin;
            string userRole;

            if (admincheck.Checked)
            {
                admin = 1;
                userRole = "admins";
            }
            else {
                admin = 0;
                userRole = "users";
            }

            MembershipUser user = Membership.GetUser(username);

            int userID;
            
            Guid aspnet_userID = (Guid)user.ProviderUserKey;
            string guidstring = Convert.ToString(aspnet_userID);

            if (Roles.RoleExists(userRole))
            {
                Roles.AddUserToRole(username, userRole);
            }
            else
            {
                throw new System.ArgumentException("Role must exist", "userRole");
            }

            CMSEverywhereService.CMSEverywhereServiceClient client = new CMSEverywhereService.CMSEverywhereServiceClient();

            string errorMessage;
            bool succeeded = client.addUser(guidstring, username, userEmail, firstname, lastname, admin, out userID, out errorMessage);
            string querystring = "";
            if (succeeded)
            {
                //querystring = "success=true&flash=User successfully created!";
                querystring = "success=true&flash=" + userEmail + " was successfully created!";

            }
            else
            {
                querystring = "success=false&flash=Failed to create user!";
            }

            Response.Redirect("~/Admin/Users.aspx?" + querystring);

        }
    }
}