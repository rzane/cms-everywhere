﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserAdd.aspx.cs" Inherits="CMSEverywhereWeb.Admin.UserAdd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="well">
    <asp:CreateUserWizard ID="CreateUserWizard1" CancelButtonStyle-CssClass="btn" StartNextButtonStyle-CssClass="btn btn-primary" CreateUserButtonStyle-CssClass="btn btn-primary" StepNextButtonStyle-CssClass="btn btn-primary" StepPreviousButtonStyle-CssClass="btn" ContinueButtonStyle-CssClass="btn btn-primary" runat="server" LoginCreatedUser="false" OnCreatedUser="CreateUserWizard1_CreatedUser">
        <WizardSteps>
            <asp:WizardStep ID="wsAddUser" runat="server">
                <asp:TextBox ID="tbFirstname" runat="server" placeholder="Firstname"  />
                <br />
                <asp:TextBox ID="tbLastname" runat="server" placeholder="Lastname" />
                <br />
                Admin:
                <asp:CheckBox ID="admincheck" runat="server" />
            </asp:WizardStep>
            <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
            </asp:CreateUserWizardStep>
            <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
            </asp:CompleteWizardStep>
        </WizardSteps>
    </asp:CreateUserWizard>
    </div>
</asp:Content>
