﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Layouts.aspx.cs" Inherits="CMSEverywhereWeb.User.Layouts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main_content">
    <div class="well">
            <asp:GridView runat="server" id="layoutlist" DataSourceID="odsLayouts" AutoGenerateColumns="false" CssClass="layoutlist hovertable table table-striped" GridLines="None">
                <Columns>
                    <asp:BoundField DataField="layoutID" HeaderText="#" />
                    <asp:BoundField DataField="layoutName" HeaderText="Layout" />
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="odsLayouts" runat="server" TypeName="CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient" SelectMethod="getLayouts"/>
    </div>
</div>
<script src="/js/layouts.js" type="text/javascript"></script>
</asp:Content>