﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="UserShow.aspx.cs" Inherits="CMSEverywhereWeb.Admin.UserShow" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main_content">
        <div class="well" id="usershow">
            <div class="show">
                <h3 id="userLogin" runat="server"></h3>
                <p><b>Email: </b><span id="userEmail" runat="server"></span></p>
                <p><b>Name: </b><span id="userFname" runat="server"></span>&nbsp;<span id="userLname" runat="server"></span></p>
                <p><b>Admin: </b><span runat="server" id="adminstringholder"></span></p>
            </div>


            <div class="edit hide">
                <p>
                    <label style="font-weight:bold;" for="usernameedit">Username: </label>
                    <asp:TextBox ID="usernameedit" runat="server"/>

                </p>
                <p>
                    <label style="font-weight:bold;" for="emailedit">Email: </label>
                    <asp:TextBox ID="emailedit" runat="server" />
                </p>
                <p>
                    <label style="font-weight:bold;" for="fnameedit">Firstname: </label>
                    <asp:TextBox ID="fnameedit" runat="server" />
                </p>
                <p>
                    <label style="font-weight:bold;" for="lnameedit">Lastname</label>
                    <asp:TextBox ID="lnameedit" runat="server" />
                </p>
                <p>
                    <label style="font-weight:bold;" for="adminedit">Admin: </label>
                    <asp:CheckBox ID="adminedit" runat="server" />
                </p>
                
                

            </div>

            <div id="buttons">
                <button class="btn btn-primary show" style="float:left;margin-right: 5px;" id="backbutton" type="button">Back</button>
                <button  type="button" id="editmode" class="btn show">Edit User</button>
                <button type="button" id="showmode" class="btn edit" style="display: none;">Cancel</button>
                <button type="button" id="update" class="edit btn btn-primary" style="display: none;">Update</button>
                <asp:Button ID="delete" runat="server" CssClass="btn btn-danger edit" style="display:none;" OnClick="delete_Click" Text="Delete"/>
            </div>
            

        </div>
    </div>
    <script>
        $(function () { prepUserShow(); });
        function prepUserShow() {
            $('#usershow #editmode').unbind('click').on('click', function () {
                $('#usershow .show').hide();
                $('#usershow .edit').show();
            });
            $('#usershow #showmode').unbind('click').on('click', function () {
                $('#usershow .edit').hide();
                $('#usershow .show').show();
            });
            $('#usershow #backbutton').unbind('click').on('click', function () {
                $('div#loading').show();
                window.location.replace("Users.aspx");
            });
        }
    </script>
</asp:Content>