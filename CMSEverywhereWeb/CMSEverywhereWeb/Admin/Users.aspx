﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="CMSEverywhereWeb.Admin.Users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div id="main_content">
        <div class="well">
            <h3 class="span4">Users</h3>
            <a href="UserAdd.aspx" style="margin-top:20px;margin-right:20px;" class="btn btn-primary pull-right" >Add User</a>
        <asp:GridView runat="server" id="userslist" DataSourceID="odsUsers" AutoGenerateColumns="false" CssClass="userslist hovertable table table-striped" GridLines="None">
            <Columns>
                <asp:BoundField HeaderText="#" DataField="userID" />
                <asp:BoundField HeaderText="Username" DataField="userLogin" />
                <asp:BoundField HeaderText="Email" DataField="userEmail" />
                <asp:BoundField HeaderText="Firstname" DataField="userFname" />
                <asp:BoundField HeaderText="Lastname" DataField="userLname" />
                <asp:BoundField HeaderText="Admin" DataField="admin" />
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="odsUsers" runat="server" TypeName="CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient" SelectMethod="getAllUsers"/>
            </div>
    </div>
    <script>
        $(document).ready(function () {
            prepUserList();
            checkForNewAdd();
        });


        bootstrap_alert = function () { }
        bootstrap_alert.warning = function (message, type) {
            var theclass;
            if (type == 'error') {
                theclass = "alert alert-error";
            } else if (type == 'success') {
                theclass = "alert alert-success";
            } else {
                theclass = "alert";
            }
            $('#alertholder').html('<div class="'+theclass+'"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>');
        }

        function checkForNewAdd() {
            var success = getURLParameter('success');
            var message = getURLParameter('flash');

            if (success == 'true') {
                bootstrap_alert.warning(message, 'success');
            } else if (success == 'false') {
                bootstrap_alert.warning(message, 'error');
            } else {
                console.log('success param not set in url');
            }

        }

        function getURLParameter(name) {
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
        }

        var userindexcontent;

        function prepUserList() {
            $('table.userslist.table tr').unbind('click').click(function () {
                var id = $(this).find('td:first').text();
                if (id != "#" && parseInt(id) > 0) {
                    $('div#loading').show();
                    var url = "UserShow.aspx?userID=" + id;

                    window.location.replace(url);
                    //var url = "UserShow.aspx?userID=" + id + " #main_content > *";
                    //userindexcontent = $('#main_content > *');
                    //$('#main_content').empty().load(url, function () {
                    //    prepUserShow();
                    //    $('div#loading').hide();
                    //}).hide().fadeIn('slow');

                }
            });
        }

        /*function prepUserShow() {
            $('#usershow #editmode').unbind('click').on('click', function () {
                $('#usershow .show').hide();
                $('#usershow .edit').show();
            });
            $('#usershow #showmode').unbind('click').on('click', function () {
                $('#usershow .edit').hide();
                $('#usershow .show').show();
            });
            $('#usershow #backbutton').unbind('click').on('click', function () {
                $('#main_content').empty().html(userindexcontent).hide().fadeIn('slow');
                prepUserList();
            });
        }*/

    </script>
</asp:Content>
