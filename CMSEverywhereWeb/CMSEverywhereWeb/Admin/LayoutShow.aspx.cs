﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace CMSEverywhereWeb.User
{
    public partial class LayoutShow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int layoutid = Convert.ToInt32(Request.QueryString["layoutID"]);
            CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient client = new CMSEverywhereService.CMSEverywhereServiceClient();
            DataSet ds = client.getLayoutByID(layoutid);

            updatebutton.Attributes.Add("data-id", layoutid.ToString());
            layoutname.InnerText = ds.Tables[0].Rows[0]["layoutName"].ToString();
            layouttext.InnerText = ds.Tables[0].Rows[0]["layout"].ToString();
        }
    }
}