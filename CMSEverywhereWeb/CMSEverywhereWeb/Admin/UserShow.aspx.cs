﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSEverywhereWeb.Admin
{
    public partial class UserShow : System.Web.UI.Page
    {
        private string username;

        protected void Page_Load(object sender, EventArgs e)
        {
            int userID = Convert.ToInt32(Request.QueryString["userID"]);
            CMSEverywhereWeb.CMSEverywhereService.CMSEverywhereServiceClient client = new CMSEverywhereService.CMSEverywhereServiceClient();
            DataSet ds = client.getUserByID(userID);

            username = Convert.ToString(ds.Tables[0].Rows[0]["userLogin"]);
            string email = Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]);
            string fname = Convert.ToString(ds.Tables[0].Rows[0]["userFname"]);
            string lname = Convert.ToString(ds.Tables[0].Rows[0]["userLname"]);
            bool admin = Convert.ToBoolean(ds.Tables[0].Rows[0]["admin"]);
            string adminstring = Convert.ToString(admin);

            userEmail.InnerText = email;
            userFname.InnerText = fname;
            userLname.InnerText = lname;
            userLogin.InnerText = username;
            adminstringholder.InnerText = adminstring;

            usernameedit.Text = username;
            fnameedit.Text = fname;
            lnameedit.Text = lname;
            emailedit.Text = email;
            adminedit.Checked = admin;

        }

        protected void delete_Click(object sender, EventArgs e)
        {
            //throw new System.ArgumentException("Username is " + username);
            Membership.DeleteUser(username);
            string querystring = "success=true&flash=" + username + " was deleted!";
            Response.Redirect("~/Admin/Users.aspx?" + querystring);
        }
    }
}