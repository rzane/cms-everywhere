﻿$(document).ready(function () {
    $(document).on('click', 'table.layoutlist tr', function () {
        var id = $(this).find('td:first').text();
        if (id != "#" && parseInt(id) > 0) {
            loadLayoutShow(id);
        }
    });
});
    
var layoutshowjsloaded = false;

function loadLayoutShow(id) {
    $('div#loading').show();
    var url = "LayoutShow.aspx?layoutID=" + id + " #main_content > *";
    $('#main_content').empty().load(url, function () {

        console.log('LAYOUT SHOW JS LOADED: ' + layoutshowjsloaded);
        if (!layoutshowjsloaded) {

            $.getScript("/js/jquery.htmlClean.min.js", function () {
                $.getScript("/js/layoutshow.js", function () {
                    layoutshowjsloaded = true;
                });
            });
        } else {
            populateEditor();
        }

        $('div#loading').hide();
    }).hide().fadeIn('slow');
}