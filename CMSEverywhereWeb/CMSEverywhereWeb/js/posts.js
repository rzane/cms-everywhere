﻿$(document).ready(function () {
    prepClicks();
});

bootstrap_alert = function () { }
bootstrap_alert.warning = function (message) {
    $('#alertholder').html('<div class="alert"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>');
}

function prepClicks() {
    //click table link
    $(document).on('click', 'table.postlist tr', function () {
        var id = $(this).find('td:first').text();
        if (id != "#" && parseInt(id) > 0) {
            loadPostShow(id);
        }
    });

    //click add post
    $(document).on('click', '#addpost', function () {
        $('.postsdiv').fadeOut('slow', function () {
            $(this).remove();
            $('.addpostdiv').fadeIn('slow');
        });
    });

    //Save new post
    $(document).on('click', '#ContentPlaceHolder1_newpostcommit', function () {
        var authorid = $(this).data('authorid');
        addPost(authorid);
    });

    //Back on new post
    $(document).on('click', '#addpostbackbutton', function () {
        loadPostIndex();
    });
}

function addPost(authorid) {
    $('div#loading').show();
    var title = $('#newposttitle').val();
    var content = $('#newpostcontent').val();
    $.post('/api/Posts', { "": "{content: '" + content + "', title: '" + title + "', authorID: " + authorid + "}" }, function (data) {
        console.log(data);
        if (Boolean(data["success"])) {
            bootstrap_alert.warning(data["title"] + " added!");
        } else {
            bootstrap_alert.warning("Failed to add post!");
        }

        $('div#loading').hide();
        setTimeout(function () {
            $('#alertholder').empty();
            loadPostIndex();
        }, 1500);
    });
}

var postshowjsloaded = false;

function loadPostShow(id) {
    $('div#loading').show();
    var url = "PostShow.aspx?postID=" + id + " #main_content > *";
    $('#main_content').empty().load(url, function () {

        console.log('POST SHOW JS LOADED: ' + postshowjsloaded);
        if (!postshowjsloaded) {
            $.getScript("/js/postshow.js", function () {
                console.log('script loaded');
                postshowjsloaded = true;
            });
        }

        $('div#loading').hide();
    }).hide().fadeIn('slow');
}