﻿$(document).ready(function () {
    prepButtons();
    prepPostHist();
});

bootstrap_alert = function () { }
bootstrap_alert.warning = function (message) {
    $('#alertholder').html('<div class="alert"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>');
}

function prepButtons() {
    //Update Button
    $(document).on('click', '#postbuttons #ContentPlaceHolder1_update', function () {
        updatePost($(this).data('id'));
    });

    //Delete Button
    $(document).on('click', '#postbuttons #delete', function () {
        deletePost();
    });

    //Edit
    $(document).on('click', '#postbuttons #editmode', function() {
        editMode();
    });
    
    //Edit Cancel
    $(document).on('click', '#postbuttons #showmode', function () {
        showMode();
    });

    //Back
    $(document).on('click', '#backbutton', function () {
        console.log('back');
        loadPostIndex();
    });
}

function loadPostIndex() {
    $('div#loading').show();
    var url = "Posts.aspx #main_content > *";
    $('#main_content').empty().load(url, function () {
        $('div#loading').hide();
    }).hide().fadeIn('slow');
}



function deletePost() {
    $('div#loading').show();
    var id = $('#ContentPlaceHolder1_update').data('id');
    $.ajax({
        url: '/api/Posts/' + id,
        type: 'DELETE',
        success: function (result) {
            console.log(result);
            if (Boolean(result["status"])) {
                bootstrap_alert.warning("Post deleted");
            } else {
                bootstrap_alert.warning("Failed to delete post!");
            }

            $('div#loading').hide();
            setTimeout(function () {
                $('#alertholder').empty();
                loadPostIndex(id);
            }, 1500);
        }
    });
}

function updatePost(id) {
    var authorid = $('#editmode').data('authorid');
    var title = $('#ContentPlaceHolder1_title').val();
    var content = $('#ContentPlaceHolder1_content').val();
    if (authorid) {
        $('div#loading').show();
        $.ajax({
            url: '/api/Posts/' + id,
            type: 'PUT',
            data: { "": "{content: '" + content + "', title: '" + title + "', authorID: " + authorid + "}" },
            success: function (data) {
                console.log(data);
                if (Boolean(data["success"])) {
                    boostrap_alert.warning(data["title"] + " successfully updated!");
                } else {
                    bootstrap_alert.warning("Failed to update post!");
                }

                $('div#loading').hide();
            }
        });
    } else {
        console.log('USER ID IS NULL!!!');
        bootstrap_alert.warning("You must be signed in to update a post!");
    }

    setTimeout(function () {
        $('#alertholder').empty();
        loadPostIndex(id);
    }, 1500);
}

function editMode() {
    $('#show').hide();
    $('#edit').show();
    $('#editmode').hide();
    $('#showmode').show();
    $('#postbuttons #delete').show();
    $('#ContentPlaceHolder1_update').show();
    $('#backbutton').hide();
}

function showMode() {
    $('#show').show();
    $('#edit').hide();
    $('#showmode').hide();
    $('#editmode').show();
    $('#ContentPlaceHolder1_update').hide();
    $('#postbuttons #delete').hide();
    $('#backbutton').show();
}

function prepPostHist() {
    $('#posthistbutton').click(function () {
        $('div#loading').show();
        var id = $('#ContentPlaceHolder1_ddlPostHist').val();
        var url = "PostHistShow.aspx?postHistID=" + id + " #main_content > *";

        $('#posthistorycontent').load(url, function () {
            $('div#loading').hide();
        }).hide().fadeIn('slow');

    });
}