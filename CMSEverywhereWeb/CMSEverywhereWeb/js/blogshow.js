﻿$(document).ready(function () {
    prepClicks();
    prepMultiselect();
});

function blogIndex() {
    $('div#loading').show();
    var url = "Blogs.aspx #main_content > *";
    $('#main_content').empty().load(url, function () {
        $('div#loading').hide();
    }).hide().fadeIn('slow');
}

function prepClicks() {
    //Update Blog
    $(document).on('click', 'button#update', function () {
        update();
    });

    $(document).on('click', '.blogshow #editmode', function () {
        $('.editbutton').show();
        $('.showbutton').hide();
        $('#show').hide();
        $('#edit').show();
    });

    $(document).on('click', '.blogshow #showmode', function () {
        $('.showbutton').show();
        $('.editbutton').hide();
        $('#edit').hide();
        $('#show').show();
    });

    $(document).on('click', '.blogshow #backbutton', function () {
        blogIndex();
    });

    $(document).on('click', '.blogshowpostlist button#addpoststoblog', function () {
        $('.blogshow, .blogshowpostlist').fadeOut('slow', function () {
            $('.blogshow, .blogshowpostlist').remove();
            $('div.addblogblogshow').fadeIn('slow');
        });
    });

    $(document).on('click', '#addposttoblogback', function () {
        var id = $('#ContentPlaceHolder1_commitpoststoblog').data('id');
        blogShow(id);
    });

    $(document).on('click', 'button#delete', function () {
        $('div#loading').show();
        deleteBlog();
    });
}

var blogshowjsloaded = false;

function blogShow(id) {
    $('div#loading').show();
    var url = "BlogShow.aspx?blogID=" + id + " #main_content > *";

    $('#main_content').empty().load(url, function () {

        if (!blogshowjsloaded) {
            $.getScript("/js/blogshow.js", function () {
                console.log('script loaded');
                blogshowjsloaded = true;
            });
        }
        $('div#loading').hide();
    }).hide().fadeIn('slow');
}

function update() {
    var name = $('#ContentPlaceHolder1_editblogname').val();
    var id = $('#ContentPlaceHolder1_editblogname').data('id');
    $('button#showmode').trigger('click');
    $.ajax({
        url: '/api/Blogs/' + id,
        type: 'PUT',
        data: { "": "{name:'" + name + "'}" },
        success: function (data) {
            console.log(data);
            $('#ContentPlaceHolder1_showblogname').fadeOut('slow', function () {
                $(this).text(data["name"]).fadeIn('slow');
            });
        }
    });
}

function deleteBlog() {
    var id = $('#ContentPlaceHolder1_editblogname').data('id');
    $.ajax({
        url: '/api/Blogs/' + id,
        type: 'DELETE',
        success: function (result) {
            console.log('RETURNED THE NEXT LINE:');
            console.log(result);
            $('div#loading').hide();

            if (result["status"]) {
                console.log('TRUE');
                bootstrap_alert.warning("Deleted");
            } else {
                console.log('The blog was not successfully deleted. Sorry!');
            };
            $('.blogshow #showmode').trigger('click');
            setTimeout(function () {
                $('#alertholder').fadeOut('slow', function () {
                    $('.blogshow #backbutton').trigger('click');
                });
            }, 1500);
        }
    });
}

function prepMultiselect() {

    var multibox = $('#multiselect');
    $.getJSON('/api/Posts', function (data) {
        $.each(data, function (index, post) {
            multibox.append($('<option>').val(post["postID"]).text(post["postTitle"]));
        });

        multibox.multiselect({
            buttonClass: 'btn',
            buttonWidth: 'auto',
            onChange: function () {
                $('#ContentPlaceHolder1_commitpoststoblog').show();
            }
        });
    });

    $('#ContentPlaceHolder1_commitpoststoblog').click(function () {
        $('div#loading').show();
        $(this).attr('disabled', 'disabled');
        var postids = multibox.val();
        var poststoadd = { "postids": postids };
        var blogID = $(this).data('id');
        $.post('/api/Blogs/AddPosts/' + blogID, { '': JSON.stringify(poststoadd) }, function (data) {
            console.log(data);
            if (data["failed"].length == 0) {
                bootstrap_alert.warning('Successfully added posts to blog');
            } else {
                bootstrap_alert.warning('Failed to add ' + data['failed'].length + ' posts to blog.');
            }
            setTimeout(function () {
                $('div#loading').hide();
                blogShow(blogID);
            }, 1500);
        });
    });
}