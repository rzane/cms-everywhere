﻿$(document).ready(function () {
    console.log('layout show!!!');
    prepClicks();

    populateEditor();
});

bootstrap_alert = function () { }
bootstrap_alert.warning = function (message) {
    $('#alertholder').html('<div class="alert"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>');
}

function prepClicks() {
    console.log('prep clicks');
    $(document).on('click', '#layoutbuttons #backbutton', function () {
        layoutIndex();
    });

    $(document).on('click', '#layoutbuttons #editbutton', function () {
        console.log('edit');
        $('.showmode').hide();
        $('.editmode').show();
        $('#show').hide();
        $('#edit').show();
    });

    $(document).on('click', '#layoutbuttons #cancelbutton', function () {
        $('.showmode').show();
        $('.editmode').hide();
        $('#show').show();
        $('#edit').hide();
    });

    $(document).on('click', '#layoutbuttons #ContentPlaceHolder1_updatebutton', function () {
        $('#layoutbuttons #ContentPlaceHolder1_updatebutton').attr('disabled', 'disabled');
        updatelayout($(this).data('id'));
    });
}

function updatelayout(id) {

    var code = $('#editor').val();

    $.ajax({
        url: '/api/Layouts/' + id,
        type: 'PUT',
        data: { "": "{layout:'"+ code +"'}" },
        success: function (data) {
            if (Boolean(data["success"])) {
                bootstrap_alert.warning("Layout successfully updated!");
            } else {
                bootstrap_alert.warning("Failed to update layout!");
            }


            setTimeout(function () {
                $('#alertholder').empty();
                layoutIndex();
            }, 1500);
        }
    });
}

function layoutIndex() {
    $('div#loading').show();
    var url = "Layouts.aspx #main_content > *";
    $('#main_content').empty().load(url, function () {
        $('div#loading').hide();
    }).hide().fadeIn('slow');
}

function populateEditor() {
    console.log('pop editor');
    var unformattedEl = $('#ContentPlaceHolder1_layouttext');

    var formatted = $.htmlClean(unformattedEl.text(), { format: true });
    unformattedEl.text(formatted);
    $('#editor').text(formatted);
}