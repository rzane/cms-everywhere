﻿$(document).ready(function () {
    prepBlogList();
    prepAdd();
});

var blogshowjsloaded;

function reinitialize() {
    $('div#loading').show();
    var url = "Blogs.aspx #main_content > *";
    $('#main_content').empty().load(url, function () {
        $('div#loading').hide();
    }).hide().fadeIn('slow');
}

function prepBlogList() {
    $(document).on('click', 'table.bloglist tr', function () {
        var id = $(this).find('td:first').text();
        if (id != "#" && parseInt(id) > 0) {
            $('div#loading').show();
            blogShow(id);
        }
    });
}

function blogShow(id) {
    var url = "BlogShow.aspx?blogID=" + id + " #main_content > *";

    $('#main_content').empty().load(url, function () {

        if (!blogshowjsloaded) {
            $.getScript("/js/blogshow.js", function () {
                console.log('script loaded');
                blogshowjsloaded = true;
            });
        }
        $('div#loading').hide();
    }).hide().fadeIn('slow');
}

bootstrap_alert = function () { }
bootstrap_alert.warning = function (message) {
    $('#alertholder').html('<div class="alert"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>');
}

function prepAdd() {
    $(document).on('click', '#addblog', function () {
        $('.blogs').empty();
        $('.blogs').append($('<h1>').text('Add Blog'));
        var nameinput = $('<input>').attr({ 'type': 'text', 'id': 'newblogname', 'placeholder': 'Name' });
        var buttonhtml = $("<p><button class='btn btn-primary' type='button' id='addblogbutton'>Add Blog</button><button class='btn' style='margin-left: 10px;' id='canceladd' type='button'>Cancel</button></p>");
        var inputshtml = $('<p>').append(nameinput).append(buttonhtml);
        $('.blogs').append(inputshtml.hide().fadeIn('slow'));
        $('.blogs').append().hide().fadeIn('slow');
    });

    $(document).on('click', '#addblogbutton', function() {
        addBlog();
    });

    $(document).on('click', '#canceladd', function () {
        reinitialize();
    });
}

function addBlog() {
    $('div#loading').show();
    var name = $('#newblogname').val();
    $.post('/api/Blogs', { '': "{name:'" + name + "'}" }, function (data) {
        $('div#loading').hide();
        if (Boolean(data["success"])) {
            bootstrap_alert.warning(data["name"] + " successfully created!");
        } else {
            bootstrap_alert.warning('An error occurred');
        }
        setTimeout(function () {
            blogShow(data["id"]);
            $('#alertholder').fadeOut('slow', function () {
                $(this).empty();
            });
        }, 1500);
    });
}


/*
function prepPostShow() {
    $('.postshow #editmode').on('click', function () {
        $('.postshow #show').hide();
        $('.postshow #edit').show();
        $('.postshow #editmode').hide();
        $('.postshow #showmode').show();
        $('.postshow #postbuttons #update').show();
        $('.postshow #postbuttons #backbutton').hide();
    });

    $('.postshow #showmode').on('click', function () {
        $('.postshow #show').show();
        $('.postshow #edit').hide();
        $('.postshow #showmode').hide();
        $('.postshow #editmode').show();
        $('.postshow #update').hide();
        $('.postshow #backbutton').show();
    });

    $('.postshow #backbutton').on('click', function () {
        $('div#loading').show();
        var url = "BlogShow.aspx?blogID=" + blogID + " #main_content > *";
        $('#main_content').empty().load(url, function () {
            prepBlogShow();
            $('div#loading').hide();
        }).hide().fadeIn('slow');

        //$('#main_content').empty().html(blogshowcontent).hide().fadeIn('slow');
    });
}
*/