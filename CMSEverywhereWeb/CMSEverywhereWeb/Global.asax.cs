﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.Http;
using System.Web.Routing;

namespace CMSEverywhereWeb
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RouteTable.Routes.MapHttpRoute(
            name: "DefaultApi",
            routeTemplate: "api/{controller}/{id}",
            defaults: new { id = System.Web.Http.RouteParameter.Optional }
            );

            RouteTable.Routes.MapHttpRoute(
                name: "ActionApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            ScriptResourceDefinition scriptResourceDefinition = new ScriptResourceDefinition();
            scriptResourceDefinition.Path = "~/js/jquery.min.js";
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery", scriptResourceDefinition);

            scriptResourceDefinition = new ScriptResourceDefinition();
            scriptResourceDefinition.Path = "~/js/bootstrap.min.js";
            ScriptManager.ScriptResourceMapping.AddDefinition("bootstrap", scriptResourceDefinition);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}