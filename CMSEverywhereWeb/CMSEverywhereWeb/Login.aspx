﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CMSEverywhereWeb.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <title>CMS Everywhere</title>
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- Le styles -->
    <link href="../css/bootstrap.css" rel="stylesheet" />
    <link href="../css/styles.css" rel="stylesheet" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet" />
    <style type="text/css">
    /* Override some defaults */
    html, body {
    background-color: #eee;
    }
    body {
    padding-top: 40px; 
    }
    .container {
    width: 300px;
    }

    /* The white background content wrapper */
    .container > .login.content {
    background-color: #fff;
    padding: 20px;
    margin: 0 -20px; 
    -webkit-border-radius: 10px 10px 10px 10px;
        -moz-border-radius: 10px 10px 10px 10px;
            border-radius: 10px 10px 10px 10px;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);
        -moz-box-shadow: 0 1px 2px rgba(0,0,0,.15);
            box-shadow: 0 1px 2px rgba(0,0,0,.15);
    }

	.login-form {
	margin-left: 65px;
	}
	
	legend {
	margin-right: -50px;
	font-weight: bold;
	color: #404040;
	}

    label {
        display: none;
    }
    
    #cmslogin_RememberMe {
        float:left;
        margin-right: 10px;
    }

    .rememberme {
        float: left;
    }
    </style>
</head>
<body>
  <div class="container">
    <div class="content">
      <div class="row">
	    <form id="form1" runat="server">
            <div class="login content">
                <div class="login-form">
                    <asp:Login ID="cmslogin" runat="server" DestinationPageUrl="~/index.aspx" OnLoggedIn="cmslogin_LoggedIn">
                    </asp:Login>
                </div>

            </div>
	    </form>
        <div style="text-align:center;margin-top:25px;">
            <p>Populate login with:</p>
            <div class="btn-group">
                <button id="userpop" class="btn">TestUser</button>
                <button id="adminpop" class="btn">TestAdmin</button>
            </div>
        </div>
      </div>
    </div>
  </div> <!-- /container -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            initialize();

            function initialize() {
                fixLogin();
                prepareClicks();
            }
            function fixLogin() {
                $('td[align="center"]').text('').append($('<h2>').text('Login'));
                $('#cmslogin_UserName').attr('placeholder', 'Username');
                $('#cmslogin_Password').attr('placeholder', 'Password');
                $('#cmslogin_LoginButton').addClass('btn btn-primary');
                $('#cmslogin_RememberMe').parent().append($('<p>').addClass('rememberme').text('Remember Me'));
            }
            function prepareClicks() {
                $('#userpop').on('click', function () {
                    popLoginInfo('user');
                });
                $('#adminpop').on('click', function () {
                    popLoginInfo('admin');
                });
            }
            function popLoginInfo(usertype) {
                var info;
                if (usertype == 'user') {
                    info = 'testuser';
                } else {
                    info = 'testadmin';
                }
                $('#cmslogin_UserName,#cmslogin_Password').val(info);
            }
        });
    </script>
</body>
</html>