use MIS2013DevelopmentGroup6;
/***********GROUPING SETS*************/
go
create procedure proc_NumberOfPostChangesByUser
as
begin
with PostInfo as (
select P.postID, max(U.userLogin) as userLogin, U.userID, count(PH.postID) as numberOfChanges
from USERS as U
inner join POSTS as P on P.postAuthorID = U.userID
inner join POST_HISTORY as PH on PH.postID = P.postID
group by PH.postID, U.userID, U.userLogin, P.postID
)

select userID, max(userLogin) as userLogin, postID, sum(numberOfChanges) as numberOfChanges from PostInfo
group by grouping sets ((userID, postID), (userID), ());
end;
/*
exec proc_NumberOfPostChangesByUser;
*/

/********ROLLUP**************/
create procedure proc_NumberOfPostChangesByUserRollUp
as
begin
with PostInfo as (
select P.postID, U.userID, max(userLogin) as userLogin, count(PH.postID) as "numberOfChanges"
from Users as U
inner join POSTS as P on P.postAuthorID = U.userID
inner join POST_HISTORY as PH on PH.postID = P.postID
group by PH.postID, U.userID, P.postID
)
select postID, userID, max(userLogin) as userLogin, sum(numberOfChanges) as numberOfChanges from PostInfo
group by userID, postID with rollup;
end;
/*
exec proc_NumberOfPostChangesByUserRollUp;
*/
/********CUBE**********/
create procedure proc_NumberOfPostChangesByUserCube
as
begin 
with PostInfo as (
select P.postID, U.userID, max(userLogin) as userLogin, count(PH.postID) as "numberOfChanges"
from Users as U
inner join POSTS as P on P.postAuthorID = U.userID
inner join POST_HISTORY as PH on PH.postID = P.postID
group by PH.postID, U.userID, P.postID
)
select postID, userID, max(userLogin) as userLogin, sum(numberOfChanges) from PostInfo
group by postID, userID with cube;
end;
/*
exec proc_NumberOfPostChangesByUserCube;
*/


/************PIVOT**********/
go

create procedure proc_PostPopPivot
as 
begin
declare @sql nvarchar(MAX);
declare @blogIDString varchar(1000)
select @blogIDString = COALESCE(@blogIDString + ',','') + QUOTENAME(blogID)
from BLOGS
set @sql = '
with PivotTable as (
select BPI.blogID, BPI.postID, ReadCounts.ReadCount
from BLOG_POST_INDEX as BPI
inner join 
	(select postID, count(postID) as "ReadCount" from POST_READ_HISTORY group by postID)
as ReadCounts on ReadCounts.postID = BPI.postID
)
select * from PivotTable
pivot
(
	sum(ReadCount)
	for blogID in (' + @blogIDString + ')) as blogID;
	'
print @sql;
exec (@sql);
end
/*
exec proc_PostPopPivot
*/

/********WINDOW************/

go

create  procedure proc_getUserPostPop
as
begin
with ReadCountTable as (
	select PRH.postID, count(PRH.postID) as "ReadCount",  max(U.userLogin) as "UserLogin" from POST_READ_HISTORY as PRH
	inner join POSTS as P on P.postID=PRH.postID
	inner join USERS as U on P.postAuthorID=U.userID
	group by PRH.postID
)
select RCT.postID, RCT.UserLogin,
MAX(RCT.ReadCount) OVER (ORDER BY RCT.ReadCount) MostPopular,
MIN(RCT.ReadCount) OVER (ORDER BY RCT.ReadCount) LeastPopular
from ReadCountTable as RCT;
end;
/*
exec proc_getUserPostPop;
*/