use MIS2013DevelopmentGroup6;
/*
drop table LAYOUT;
drop table POST_READ_HISTORY;
drop table BLOG_POST_INDEX;
drop table IMAGES;
drop table BLOGS;
drop table GOOGLE_ANALYTICS;
drop table POST_HISTORY;
drop table POSTS;
drop table USERS;
*/

CREATE TABLE USERS (
  userID int PRIMARY KEY IDENTITY,
  userLogin varchar(16) NOT NULL,
  userPass varchar(16) NOT NULL,
  userEmail varchar(100) NOT NULL,
  userFname varchar(25) NOT NULL,
  userLname varchar(25) NOT NULL,
  admin bit NOT NULL
)

CREATE TABLE POSTS (
  postID int PRIMARY KEY IDENTITY,
  postDate datetime NOT NULL,
  postContent nvarchar(MAX),
  postTitle nvarchar(MAX),
  postAuthorID int NOT NULL,
  CONSTRAINT fk_PostAuthorID FOREIGN KEY (postAuthorID) REFERENCES USERS(userID) ON DELETE CASCADE
)

CREATE TABLE POST_HISTORY (
  postHistoryID int PRIMARY KEY IDENTITY,
  postID int NOT NULL,
  postDate datetime NOT NULL,
  postContent nvarchar(MAX),
  postTitle nvarchar(MAX),
  postAuthorID int NOT NULL,
  CONSTRAINT fk_PostAuthorID_PostHistory FOREIGN KEY (postAuthorID) REFERENCES USERS(userID),
  CONSTRAINT fk_PostID_PostHistory FOREIGN KEY (postID) REFERENCES POSTS(postID) ON DELETE CASCADE
)

CREATE TABLE POST_READ_HISTORY (
	postReadHistoryID int PRIMARY KEY IDENTITY,
	postID int NOT NULL,
	postReadDate datetime NOT NULL
	CONSTRAINT fk_PostID_PostReadHistory FOREIGN KEY (postID) REFERENCES POSTS(postID) ON DELETE CASCADE
)

CREATE TABLE GOOGLE_ANALYTICS (
	gaID int PRIMARY KEY IDENTITY,
	gaLogin varchar(60) NOT NULL,
	gaPass varchar(60) NOT NULL
)

CREATE TABLE BLOGS (
	blogID int PRIMARY KEY IDENTITY,
	blogName varchar(100) NOT NULL,	
)

CREATE TABLE IMAGES (
	imageID int PRIMARY KEY IDENTITY,
	imagePath varchar(300) NOT NULL,
	imageCaption varchar(300) NOT NULL,
	imageAuthorID int NOT NULL,
	CONSTRAINT fk_ImageAuthorID FOREIGN KEY (imageAuthorID) REFERENCES USERS(userID) ON DELETE CASCADE
)

CREATE TABLE BLOG_POST_INDEX (
	blogID int,
	postID int,
	CONSTRAINT fk_BlogID_Index FOREIGN KEY (blogID) REFERENCES BLOGS(blogID) ON DELETE CASCADE,
	CONSTRAINT fk_PostID_Index FOREIGN KEY (postID) REFERENCES POSTS(postID) ON DELETE CASCADE
)

CREATE TABLE LAYOUT (
	layoutID int PRIMARY KEY IDENTITY,
	layoutName varchar(25) NOT NULL,
	layout nvarchar(MAX) NOT NULL
)
