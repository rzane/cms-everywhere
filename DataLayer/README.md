CMS Everywhere
==============

## Description
CMS System built in ASP.NET (not by choice, by university requirement :disappointed:). It will allow users to use dynamic content on static websites. Using AJAX, users will be able to include dynamic content in their websites without the use of a server-sided language.

## Project Status
:white_check_mark: Database Designed  
:white_check_mark: Data Layer Objects Created  
:soon: Beginning of ASP.NET Development
