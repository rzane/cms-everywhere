use MIS2013DevelopmentGroup6;

go

alter table USERS add aspnet_userId uniqueidentifier;

alter table USERS add constraint fkUserToAspnet_Users foreign key(aspnet_userId) references aspnet_Users(UserId); 


create procedure proc_getImages
as
begin
select * from IMAGES;
end
/*
execute proc_getImages;
*/
go

create procedure proc_getPosts
as
begin
select postID, postDate, postContent, postTitle, userLogin from POSTS as P
inner join USERS as U on U.userID = P.postAuthorID;
end
/*
exec proc_getPosts;
*/

go

create procedure proc_getBlogByID (
@blogID int
)
as
begin
select blogName from BLOGS where blogID=@blogID;
end
/*
exec proc_getBlogByID @blogID=1
*/

go

create procedure proc_getBlogs
as
begin
select * from BLOGS;
end
/*
exec proc_getBlogs;
*/
go

create procedure proc_getAllGA
as
begin
select * from GOOGLE_ANALYTICS;
end
/*
exec proc_getAllGA
*/
go

create procedure proc_getAllUsers
as
begin
select * from USERS;
end
/*
exec proc_getAllUsers
*/
go

create procedure proc_getImagebyID (
@imageID int
)
as
begin
select imagePath, imageCaption, imageAuthorID from IMAGES where imageID=@imageID;
end
/*
exec proc_getImagebyID @imageID=1;
*/

go

create procedure proc_getGAbyID (
@gaID int
)
as
begin
select gaLogin, gaPass from GOOGLE_ANALYTICS where gaID=@gaID;
end
/*
exec proc_getGAbyID
@gaID = 1;
*/
go

create procedure proc_getUserByID (
@userID int
)
as
begin
select userLogin, userPass, userEmail, userFname, userLname, admin from USERS where userID=@userID;
end
/*
exec proc_getUserByID @userID=1;
*/

go

create procedure proc_updateImage (
@imageID int,
@imagePath varchar(300) = null,
@imageCaption varchar(300) = null,
@imageAuthorID int = null,
@errorMessage nvarchar(MAX) out
)
as
begin
declare @succeeded bit;
begin try
update IMAGES set imagePath=ISNULL(@imagePath, imagePath), imageAuthorID=ISNULL(@imageAuthorID, imageAuthorID), imageCaption=ISNULL(@imageCaption, imageCaption) where imageID=@imageID;
if (@@ROWCOUNT = 1)
set @succeeded = 1;
else
set @succeeded = 0;
end try
begin catch
if (ERROR_MESSAGE() like '%fk_ImageAuthorID%')
	begin
		set @errorMessage = 'Invalid User'
	end
end catch
return @succeeded;
end;
/*
declare @succeeded bit;
declare @errorMessage nvarchar(100);
exec @succeeded = proc_updateImage @imagepath = "image/new/path/cool/dude", @imageCaption = "Best Picture EVER", @imageID = 2, @errorMessage = @errorMessage output;
print @succeeded;
print @errorMessage;
*/

go

create procedure proc_updateBlog (
@blogID int,
@blogName varchar(100) = null
)
as
begin
declare @succeeded bit;
begin try
update BLOGS set blogName=ISNULL(@blogName, blogName) where blogID=@blogID;
if (@@ROWCOUNT = 1)
set @succeeded = 1;
else
set @succeeded = 0;
end try
begin catch
end catch
return @succeeded;
end;
/*
declare @succeeded bit;
exec @succeeded = proc_updateBlog @blogID=1, @blogName="Updated Blog Name";
print @succeeded;
*/


go

create procedure proc_updateGALoginInfo (
@gaID int,
@gaLogin Varchar(60) = null,
@gaPass varchar(60) = null
)
as
begin
declare @succeeded bit;
begin try
update GOOGLE_ANALYTICS set gaLogin=ISNULL(@gaLogin, gaLogin), gaPass=ISNULL(@gaPass, gaPass) where gaID=@gaID;
if (@@ROWCOUNT = 1)
set @succeeded = 1;
else
set @succeeded = 0;
end try
begin catch
end catch
return @succeeded;
end;
/*
declare @succeeded bit;
exec @succeeded = proc_updateGALoginInfo @gaLogin = "raymondzane@gmail.com", @gaPass="UpdatedPass", @gaID=1;
print @succeeded;
*/
go

create procedure proc_updateUser (
@userID int,
@userLogin varchar(16) = null,
@userPass Varchar(16) = null,
@userEmail varchar(100) = null,
@admin bit = null
)
as
begin
declare @succeeded bit;
begin try
update USERS set userLogin=ISNULL(@userLogin, userLogin), userPass=ISNULL(@userPass, userPass), userEmail=ISNULL(@userEmail, userEmail), admin=ISNULL(@admin, admin) where userID=@userID;
if (@@ROWCOUNT = 1)
set @succeeded = 1;
else
set @succeeded = 0;
end try
begin catch
end catch
return @succeeded;
end;
/*
declare @succeeded bit;
exec @succeeded = proc_updateUser @userID = 1, @userLogin="rzane", @userPass="newPass", @userEmail="raymondzane@gmail.com", @admin=1;
print @succeeded;
*/
go

create procedure proc_deleteImage (
@imageID int,
@errorMessage nvarchar(100) output
)
as
begin
declare @succeeded bit;
set @succeeded = 1;
set @errorMessage = 'No errors';
begin try
delete from IMAGES where imageID=@imageID;
end try
begin catch
set @succeeded = 0;
if (ERROR_MESSAGE() like '%fk_ImageAuthorID%')
begin
	set @errorMessage = 'Violation of Constraint';
end
else
	set @errorMessage = 'Unknown Error Occurred';
end catch
return @succeeded;
end;
/*
declare @succeeded bit, @errorMessage nvarchar(100);
exec @succeeded = proc_deleteImage @imageID = 1, @errorMessage = @errorMessage out;
print @succeeded;
print @errorMessage;
*/
go

create procedure proc_deletePost (
@postID int,
@errorMessage nvarchar(100) output
)
as
begin
declare @succeeded bit;
set @errorMessage = 'No errors';
set @succeeded = 1;
begin try
--delete from POST_HISTORY where postID=@postID;
delete from POSTS where postID=@postID;
end try
begin catch
set @succeeded = 0;
if (ERROR_MESSAGE() like '%fk_PostAuthorID%')
begin
	set @errorMessage = 'Violation of Constraint';
end
else
	set @errorMessage = 'Unknown Error Occurred';
end catch
return @succeeded;
end;
/*
declare @succeeded bit, @errorMessage nvarchar(100);
exec @succeeded = proc_deletePost @postID=2, @errorMessage = @errorMessage out;
print @succeeded;
print @errorMessage;
select * from POSTS;
*/
go

create procedure proc_deleteBlog (
@blogID int,
@errorMessage nvarchar(100) output
)
as
begin
declare @succeeded bit;
set @errorMessage = 'No errors';
set @succeeded = 1;
begin try
delete from BLOGS where blogID=@blogID;
end try
begin catch
set @succeeded = 0;
set @errorMessage = 'Error occurred';
end catch
return @succeeded;
end;
/*
declare @succeeded bit, @errorMessage nvarchar(100);
exec @succeeded = proc_deleteBlog @blogID=4, @errorMessage = @errorMessage out;
print @succeeded;
print @errorMessage;
*/

go

create procedure proc_deleteGALoginInfo (
@gaID int,
@errorMessage nvarchar(100) output
)
as
begin
declare @succeeded bit;
set @errorMessage = 'No errors';
set @succeeded = 1;
begin try
delete from GOOGLE_ANALYTICS where gaID=@gaID;
end try
begin catch
set @succeeded = 0;
set @errorMessage = 'Error occurred';
end catch
return @succeeded;
end;
/*
declare @succeeded bit, @errorMessage nvarchar(100);
exec @succeeded = proc_deleteGALoginInfo @gaID=1, @errorMessage=@errorMessage output;
print @succeeded;
print @errorMessage;
*/
go

create procedure proc_deleteUser (
@userID int,
@errorMessage nvarchar(100) output
)
as
begin
declare @succeeded bit;
set @errorMessage = 'No errors';
set @succeeded = 1;
begin try
delete from USERS where userID=@userID;
end try
begin catch
set @succeeded = 0;
set @errorMessage = 'Error occurred';
end catch
return @succeeded;
end;
/*
declare @succeeded bit, @errorMessage nvarchar(100);
exec @succeeded = proc_deleteUser @userID=1, @errorMessage=@errorMessage output;
print @succeeded;
print @errorMessage;
delete from USERS where userID=1;
*/

go

alter procedure proc_addImage (
@imagePath Varchar(300),
@imageCaption Varchar(300),
@imageAuthorID Int,
@imageID int output,
@errorMessage nvarchar(100) output
)
as
begin
declare @succeeded bit;
set @succeeded = 1;

begin try
INSERT [dbo].[IMAGES] ([imagePath], [imageCaption], [imageAuthorID]) VALUES (@imagePath, @imageCaption, @imageAuthorID);
set @imageID = IDENT_CURRENT('IMAGES');

end try
begin catch
set @succeeded = 0;
if (ERROR_MESSAGE() like '%fk_ImageAuthorID%')
	begin
		set @errorMessage = 'Violation of constraint';
	end
else
	set @errorMessage = 'Error occurred';
end catch
return @succeeded;
end;
/*
declare @succeeded bit, @errorMessage nvarchar(100), @imageID int;
exec @succeeded = proc_addImage @imagePath = "get/that/image", @imageCaption = "imageCaption", @imageAuthorID = 2, @errorMessage = @errorMessage output, @imageID=@imageID output;
print @succeeded;
print @errorMessage;
print @imageID;
*/
go


alter procedure proc_addPost (
@postContent nvarchar(MAX),
@postTitle nvarchar(MAX),
@postAuthorID int,
@postID int output,
@errorMessage nvarchar(100) output
)
as
begin
declare @succeeded bit;
set @succeeded = 1;
set @errorMessage = 'No errors';
begin try
	INSERT [dbo].[POSTS] ([postDate], [postContent], [postTitle], [postAuthorID]) VALUES (GETDATE(), @postContent, @postTitle, @postAuthorID);
	set @postID = IDENT_CURRENT('POSTS');
end try
begin catch
	set @succeeded = 0;
	IF (ERROR_MESSAGE() like '%fk_PostAuthorID%')
			set @errorMessage = 'Invalid User'
	ELSE
			set @errorMessage = 'Error occurred';
end catch
return @succeeded;
end;
/*
declare @succeeded bit, @errorMessage nvarchar(100), @postID int;
exec @succeeded = proc_addPost @postContent='This is a new post', @postTitle='New Post', @postAuthorID=2, @errorMessage=@errorMessage output, @postID=@postID output;
print @succeeded;
print @errorMessage;
print @postID;
*/


go

create procedure proc_addBlog (
@blogName Varchar(100),
@errorMessage nvarchar(100) output
)
as
begin
declare @succeeded bit;
set @errorMessage = 'No errors';
set @succeeded = 1;
begin try
INSERT [dbo].[BLOGS] ([blogName]) VALUES (@blogName)
end try
begin catch
set @succeeded = 0;
set @errorMessage = 'Error occurred';
end catch
return @succeeded;
end;
/*
declare @succeeded bit, @errorMessage nvarchar(100);
exec @succeeded = proc_addBlog @blogName="New Blog", @errorMessage=@errorMessage output;
print @succeeded; 
print @errorMessage;
*/
go

create procedure proc_addGALoginInfo (
@gaLogin Varchar(60),
@gaPass varchar(60),
@errorMessage nvarchar(100) output
)
as
begin
declare @succeeded bit;
set @errorMessage = 'No errors';
set @succeeded = 1;
begin try
insert into GOOGLE_ANALYTICS (gaLogin, gaPass) values (@gaLogin, @gaPass);
end try
begin catch
set @succeeded = 0;
set @errorMessage = 'Error occurred';
end catch
return @succeeded;
end;
/*
declare @succeeded bit, @errorMessage nvarchar(100);
exec @succeeded = proc_addGALoginInfo @gaLogin="raymondzane@gmail.com", @gaPass="testpass", @errorMessage=@errorMessage output;
print @succeeded;
print @errorMessage;
*/
go

create procedure proc_addUser (
@userLogin varchar(16),
@userPass Varchar(16),
@userEmail varchar(100),
@userFName varchar(25),
@userLName varchar(25),
@admin bit,
@errorMessage nvarchar(100) output
)
as
begin
declare @succeeded bit;
set @succeeded = 1;
set @errorMessage = 'No errors';
begin try
INSERT [dbo].[USERS] ([userLogin], [userPass], [userEmail], [userFname], [userLname], [admin]) VALUES (@userLogin, @userPass, @userEmail, @userFName, @userLName, @admin);
end try
begin catch
set @succeeded = 0;
set @errorMessage = 'Error occurred';
end catch
return @succeeded;
end;
/*
declare @succeeded bit, @errorMessage nvarchar(100);
exec @succeeded = proc_addUser @userLogin = "testlogin", @userPass = "testpassword", @userEmail = "Gates@Microsoft.com", @userFName = "Bill", @userLName = "Gates", @admin = 1, @errorMessage = @errorMessage output;
print @succeeded;
print @errorMessage;
*/
go

go

create procedure proc_updateLayout (
@layoutID int,
@layoutName varchar(25) = null,
@layout nvarchar(MAX) = null
)
as
begin
declare @succeeded bit;
begin try
update LAYOUT set layout=ISNULL(@layout, layout) where layoutID=@layoutID;
if (@@ROWCOUNT = 1)
set @succeeded = 1;
else
set @succeeded = 0;
end try
begin catch
end catch
return @succeeded;
end;
/*
declare @succeeded bit;
exec @succeeded = proc_updateLayout @layoutID=1, @layout="<div></div>";
print @succeeded;
*/
select * from LAYOUT;
go

create procedure proc_getLayouts
as
begin
select * from LAYOUT;
end
/*
exec proc_getLayouts;
*/
go


create procedure proc_getLayoutByID (
@layoutID int
)
as
begin
select layoutName, layout from LAYOUT where layoutID=@layoutID;
end
/*
exec proc_getLayoutByID @layoutID = 1;
*/
go

create procedure proc_getPostHistoryTitleByPostID (
@postID int
)
as
begin
select PostHistoryID, postTitle, postDate from POST_HISTORY where postID=@postID;
end
/*
exec proc_getPostHistoryTitleByPostID @postID = 1;
*/
go

create procedure proc_getPostHistoryByPostHistID (
@postHistoryID int
)
as
begin
select postDate, postContent, postTitle, postAuthorID from POST_HISTORY where postHistoryID=1;
end
/*
exec proc_getPostHistoryByPostHistID @postHistoryID=1;
*/
go

create function fn_getUserPrivilege (@userID int)
returns bit
as
begin
DECLARE @admin bit
SET @admin = (select admin from USERS where userID=@userID);
return @admin
end
/*
SELECT dbo.fn_getUserPrivilege(4) as admin;
*/
go

create procedure proc_addPostReadHistory (
@postID int,
@errorMessage nvarchar(100) output
)
as
begin
declare @succeeded bit;
begin try
INSERT [dbo].[POST_READ_HISTORY] ([postID], [postReadDate]) VALUES (@postID, GETDATE());
set @succeeded = 1;
end try
begin catch
if (ERROR_MESSAGE() like '%fk_PostID_PostReadHistory%')
begin
	set @errorMessage = 'Violation of Constraint'
end
else
	set @errorMessage = 'Unknown Error Occurred'
set @succeeded = 0;
end catch
return @succeeded;
end;

/*
declare @succeeded bit;
declare @errorMessage nvarchar(100);
exec @succeeded = proc_addPostReadHistory @postID = 1, @errorMessage = @errorMessage output;
print @succeeded;
print @errorMessage;
*/

go

create procedure proc_getPostbyID (
@postID int,
@readerORauthor int --we only want to call proc_addtoPostReadHistory if the post is being displayed on the frontend
)
as
begin
	select postID, postDate, postContent, postTitle, postAuthorId from POSTS where postID=@postID;
	if (@readerORauthor=0)
		begin
		declare @succeeded bit;
		exec @succeeded = proc_addPostReadHistory @postID=@postID
		return @succeeded;
	end
end
/*
declare @succeeded bit;
exec proc_getPostbyID @postID=1, @readerORauthor=1; --if readorORauthor is 1, POST_READ_HISTORY won't be updated
print @succeeded;
select * from POST_READ_HISTORY;
*/

go

create procedure proc_addPostHistory (
@postID int,
@oldpostContent nvarchar(MAX),
@oldpostTitle nvarchar(MAX),
@oldpostAuthorID int,
@errorMessage nvarchar(100) output
)
as
begin
declare @succeeded bit;
begin try
INSERT [dbo].[POST_HISTORY] ([postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (@postID, GETDATE(), @oldpostContent, @oldpostTitle, @oldpostAuthorID);
set @succeeded = 1;
end try
begin catch
set @succeeded = 0;
	if (ERROR_MESSAGE() like '%fk_PostAuthorID_PostHistory%')
		begin
			set @errorMessage = 'Violation of constraint';
		end
	if (ERROR_MESSAGE() like '%fk_PostID_PostHistory%')
		begin
			set @errorMessage = 'Violation of constraint';
		end
	else
		set @errorMessage = 'No error'
end catch
return @succeeded;
end;
/*
declare @succeeded bit;
declare @errorMessage nvarchar(100);
exec @succeeded = proc_addPostHistory @postID=1, @oldpostContent="Old Post content", @oldpostTitle="Old Post Title", @oldpostAuthorID=1, @errorMessage = @errorMessage output;
print @errorMessage;
print @succeeded;
*/


go

create procedure proc_updatePost (
	@postID int,
	@postContent nvarchar(MAX) = null,
	@postTitle nvarchar(MAX) = null,
	@postAuthorID int = null,
	@errorMessage nvarchar(MAX) out
)
as
begin

	declare @succeeded bit;
	declare @postHistSucceeded bit;
	declare @oldpostContent nvarchar(MAX);
	declare @oldpostTitle nvarchar(MAX);
	declare @oldpostAuthorID int;
	declare @historyErrorMessage nvarchar(MAX) = null;

	set @succeeded = 1;

	/*
	Try to record post into POST_HISTORY table
	if it fails, set succeeded to 0 and errorMessage to the addPostHistory error message
	*/
	begin try
			select @oldpostContent = postContent,
			@oldpostTitle = postTitle,
			@oldpostAuthorID = postAuthorID from POSTS where postID=@postID;

			exec @postHistSucceeded = proc_addPostHistory @postID=@postID, @oldpostContent=@oldpostContent, @oldpostTitle=@oldpostTitle, 				@oldpostAuthorID=@oldpostAuthorID, @errorMessage=@historyErrorMessage;
	end try
	begin catch
		set @succeeded = 0;
		set @errorMessage = 'Post History Save Failed: ' + @historyErrorMessage;			
	end catch

	if (@postHistSucceeded = 1)
	BEGIN
		begin try
			update POSTS set postContent=ISNULL(@postContent,postContent), postTitle=ISNULL(@postTitle,postTitle),
			postAuthorID=ISNULL(@postAuthorID, postAuthorID) where postID=@postID;
		end try
		begin catch
			if (ERROR_MESSAGE() like '%fk_PostAuthorID%')
			BEGIN
				set @succeeded = 0;
				set @errorMessage = 'Invalid User';
			END
			ELSE
			BEGIN
				set @succeeded = 0;
				set @errorMessage = 'Unknown Update Error';
			END
		end catch
	END
	else
	BEGIN
		set @succeeded = 0;
	END
	return @succeeded;
end;
/*
declare @succeeded bit;
declare @errorMessage nvarchar(MAX);
exec @succeeded = proc_updatePost @postID=1,
@postContent="Updated Content",
@postTitle="Another New Updated Title",
@postAuthorID=1, @errorMessage=@errorMessage out;
print @succeeded;
print @errorMessage;
*/


go

create procedure proc_getPostsByBlog (
	@blogID int
)
as
begin
	select BPI.postID, postDate, postContent, postTitle, userLogin from BLOG_POST_INDEX as BPI
	inner join POSTS as P on P.postID = BPI.postID
	inner join USERS as U on U.userID = P.postAuthorID
	where BPI.blogID = @blogID;
end;
/*
exec proc_getPostsByBlog @blogID=1;
*/

go

create procedure proc_addPostToBlog (
	@postID int,
	@blogID int,
	@errorMessage nvarchar(100) output
)
as
begin
	declare @succeeded bit;
	begin try
	INSERT [dbo].[BLOG_POST_INDEX] ([blogID], [postID]) VALUES (@blogID, @postID);
	set @succeeded = 1;
	end try
	begin catch
	set @succeeded = 0;
	if (ERROR_MESSAGE() like '%fk_BlogID_Index%')
	begin
		set @errorMessage = 'Blog ID Constraint Violation'
	end
	else if (ERROR_MESSAGE() like '%fk_PostID_Index%')
		set @errorMessage = 'Post ID Constraint Violation'
	else
		set @errorMessage = 'Unknown Error Occurred'
	end catch
	return @succeeded;
end
/*
declare @succeeded bit;
declare @errorMessage nvarchar(100);
exec @succeeded = proc_addPostToBlog @blogID=1, @postID=1, @errorMessage = @errorMessage output;
print @succeeded;
print @errorMessage;
*/

