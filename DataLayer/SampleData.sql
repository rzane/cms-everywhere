USE [MIS2013DevelopmentGroup6]
GO
SET IDENTITY_INSERT [dbo].[USERS] ON 

INSERT [dbo].[USERS] ([userID], [userLogin], [userPass], [userEmail], [userFname], [userLname], [admin]) VALUES (1, N'cclark', N'password', N'cclark23@mix.wvu.edu', N'chris', N'clark', 1)
INSERT [dbo].[USERS] ([userID], [userLogin], [userPass], [userEmail], [userFname], [userLname], [admin]) VALUES (2, N'rzane', N'password', N'rzane@mix.wvu.edu', N'ray', N'zane', 0)
INSERT [dbo].[USERS] ([userID], [userLogin], [userPass], [userEmail], [userFname], [userLname], [admin]) VALUES (4, N'nick', N'password', N'nick@mix.wvu.edu', N'nick', N'timpanelli', 1)
INSERT [dbo].[USERS] ([userID], [userLogin], [userPass], [userEmail], [userFname], [userLname], [admin]) VALUES (6, N'tim', N'password', N'tim@mix.wvu.edu', N'tim', N'barlow', 0)
INSERT [dbo].[USERS] ([userID], [userLogin], [userPass], [userEmail], [userFname], [userLname], [admin]) VALUES (7, N'stewy', N'password', N'stewygriff@yahoo.com', N'stewy', N'griffin', 0)
SET IDENTITY_INSERT [dbo].[USERS] OFF
SET IDENTITY_INSERT [dbo].[POSTS] ON 

INSERT [dbo].[POSTS] ([postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (1, CAST(0x0000A10400000000 AS DateTime), N'fuzzy wuzzy was a bear, fuzzy wuzzy had no hair. fuzzy wuzzy wasnt fuzzy was he?', N'Questions of life', 6)
INSERT [dbo].[POSTS] ([postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (2, CAST(0x0000A05900000000 AS DateTime), N'gotta catch em all', N'pokemon', 1)
INSERT [dbo].[POSTS] ([postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (5, CAST(0x0000A14200000000 AS DateTime), N'lemon-lime soda 100% natural flavors no caffeine', N'sprite', 4)
INSERT [dbo].[POSTS] ([postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (8, CAST(0x0000A05A00000000 AS DateTime), N'hello hello good bye', N'pennies', 2)
INSERT [dbo].[POSTS] ([postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (9, CAST(0x0000A06F00000000 AS DateTime), N'bleepidy bleepidy bleepidy bleep', N'blurp', 1)
INSERT [dbo].[POSTS] ([postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (10, CAST(0x0000A09C00000000 AS DateTime), N'rapunzel rapunzle let down your brown hair', N'rapunzel', 7)
INSERT [dbo].[POSTS] ([postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (11, CAST(0x0000A0B000000000 AS DateTime), N'happy happy birthday from all of us to you, we wish it were our birthday from chevys crew to you! Hey!', N'Birthday Song', 2)
INSERT [dbo].[POSTS] ([postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (12, CAST(0x0000A0DA00000000 AS DateTime), N'I have a PHD in underwater basket weaving', N'about me', 6)
INSERT [dbo].[POSTS] ([postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (13, CAST(0x0000A12500000000 AS DateTime), N'the snack that smiles back', N'goldfish', 4)
SET IDENTITY_INSERT [dbo].[POSTS] OFF

SET IDENTITY_INSERT [dbo].[POST_HISTORY] ON 

INSERT [dbo].[POST_HISTORY] ([postHistoryID], [postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (1, 1, CAST(0x0000A10400000000 AS DateTime), N'fuzzy wuzzy was a bear, fuzzy wuzzy had no hair. fuzzy wuzzy wasnt fuzzy was he?', N'Questions of life', 6)
INSERT [dbo].[POST_HISTORY] ([postHistoryID], [postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (2, 2, CAST(0x0000A05900000000 AS DateTime), N'gotta catch em all', N'pokemon', 1)
INSERT [dbo].[POST_HISTORY] ([postHistoryID], [postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (3, 5, CAST(0x0000A14200000000 AS DateTime), N'lemon-lime soatural flavors no caffeine', N'sprite', 4)
INSERT [dbo].[POST_HISTORY] ([postHistoryID], [postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (4, 8, CAST(0x0000A05A00000000 AS DateTime), N'hello hello ge', N'pennies', 2)
INSERT [dbo].[POST_HISTORY] ([postHistoryID], [postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (5, 9, CAST(0x0000A06F00000000 AS DateTime), N'bleepidy bleey bleepidy bleep', N'blurp', 1)
INSERT [dbo].[POST_HISTORY] ([postHistoryID], [postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (6, 10, CAST(0x0000A09C00000000 AS DateTime), N'rapunzel raple let down your brown hair', N'rapunzel', 7)
INSERT [dbo].[POST_HISTORY] ([postHistoryID], [postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (7, 11, CAST(0x0000A0B000000000 AS DateTime), N'happy happy  from all of us to you, we wish it were our birthday from chevys crew to you! Hey!', N'Birthday Song', 2)
INSERT [dbo].[POST_HISTORY] ([postHistoryID], [postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (8, 12, CAST(0x0000A0DA00000000 AS DateTime), N'I have a PHDunderwater basket weaving', N'about me', 6)
INSERT [dbo].[POST_HISTORY] ([postHistoryID], [postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (9, 13, CAST(0x0000A12500000000 AS DateTime), N'the snack smiles back', N'goldfish', 4)
INSERT [dbo].[POST_HISTORY] ([postHistoryID], [postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (10, 2, CAST(0x0000A05900000000 AS DateTime), N'gotta catc=======all', N'pokemon', 1)
INSERT [dbo].[POST_HISTORY] ([postHistoryID], [postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (11, 2, CAST(0x0000A05900000000 AS DateTime), N'gotta catch em al\dsfdafl', N'pokemon', 1)
INSERT [dbo].[POST_HISTORY] ([postHistoryID], [postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (12, 12, CAST(0x0000A0DA00000000 AS DateTime), N'I have a PHD asdfasin underwater basket weaving', N'about me', 6)
INSERT [dbo].[POST_HISTORY] ([postHistoryID], [postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (13, 8, CAST(0x0000A05A00000000 AS DateTime), N'hello dfghdfghello good bye', N'pennies', 2)
INSERT [dbo].[POST_HISTORY] ([postHistoryID], [postID], [postDate], [postContent], [postTitle], [postAuthorID]) VALUES (14, 8, CAST(0x0000A05A00000000 AS DateTime), N'hello hellodfgjdgds good bye', N'pennies', 2)
SET IDENTITY_INSERT [dbo].[POST_HISTORY] OFF

SET IDENTITY_INSERT [dbo].[IMAGES] ON 

INSERT [dbo].[IMAGES] ([imageID], [imagePath], [imageCaption], [imageAuthorID]) VALUES (1, N'window/program/picture/image', N'scooby snacks', 1)
INSERT [dbo].[IMAGES] ([imageID], [imagePath], [imageCaption], [imageAuthorID]) VALUES (2, N'/path/to/image', N'this is a cool picture', 2)
INSERT [dbo].[IMAGES] ([imageID], [imagePath], [imageCaption], [imageAuthorID]) VALUES (4, N'/path/to/image2', N'here''s another cool picture', 4)
INSERT [dbo].[IMAGES] ([imageID], [imagePath], [imageCaption], [imageAuthorID]) VALUES (5, N'/path/to/image2', N'this is yet another image', 6)
INSERT [dbo].[IMAGES] ([imageID], [imagePath], [imageCaption], [imageAuthorID]) VALUES (6, N'/path/to/image3', N'Another Caption', 2)
INSERT [dbo].[IMAGES] ([imageID], [imagePath], [imageCaption], [imageAuthorID]) VALUES (7, N'/path/to/image4', N'Yet Another Caption', 4)
SET IDENTITY_INSERT [dbo].[IMAGES] OFF
SET IDENTITY_INSERT [dbo].[BLOGS] ON 

INSERT [dbo].[BLOGS] ([blogID], [blogName]) VALUES (1, N'Blog A')
INSERT [dbo].[BLOGS] ([blogID], [blogName]) VALUES (2, N'Blog B')
SET IDENTITY_INSERT [dbo].[BLOGS] OFF
INSERT [dbo].[BLOG_POST_INDEX] ([blogID], [postID]) VALUES (1, 1)
INSERT [dbo].[BLOG_POST_INDEX] ([blogID], [postID]) VALUES (2, 2)
INSERT [dbo].[BLOG_POST_INDEX] ([blogID], [postID]) VALUES (1, 5)
INSERT [dbo].[BLOG_POST_INDEX] ([blogID], [postID]) VALUES (2, 8)
INSERT [dbo].[BLOG_POST_INDEX] ([blogID], [postID]) VALUES (1, 10)
INSERT [dbo].[BLOG_POST_INDEX] ([blogID], [postID]) VALUES (2, 11)
INSERT [dbo].[BLOG_POST_INDEX] ([blogID], [postID]) VALUES (1, 13)
SET IDENTITY_INSERT [dbo].[GOOGLE_ANALYTICS] ON 

INSERT [dbo].[GOOGLE_ANALYTICS] ([gaID], [gaLogin], [gaPass]) VALUES (1, N'raymondzane@gmail.com', N'test123')
SET IDENTITY_INSERT [dbo].[GOOGLE_ANALYTICS] OFF

SET IDENTITY_INSERT [dbo].[POST_READ_HISTORY] ON 
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (1, 1, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (2, 1, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (3, 1, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (4, 1, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (5, 2, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (6, 2, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (7, 5, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (8, 5, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (9, 5, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (10, 5, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (11, 5, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (12, 8, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (13, 8, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (14, 9, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (15, 9, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (16, 9, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (17, 9, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (18, 9, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (19, 9, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (20, 10, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (21, 10, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (22, 10, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (23, 11, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (24, 12, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (25, 12, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (26, 12, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (27, 12, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (28, 12, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (29, 12, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (30, 12, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (31, 12, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (32, 12, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (33, 13, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (34, 13, CAST(0x0000A05A00000000 AS DateTime))
INSERT [dbo].[POST_READ_HISTORY] ([postReadHistoryID], [postID], [postReadDate]) VALUES (35, 13, CAST(0x0000A05A00000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[POST_READ_HISTORY] OFF

SET IDENTITY_INSERT [dbo].[LAYOUT] ON 

INSERT [dbo].[LAYOUT] ([layoutID], [layoutName], [layout]) VALUES (1, N'All Blog Posts', N'<div class="blogpost"><div class="posttitle">{title}</div><div class="date">{date}</div><hr><p class="postcontent">{content}</p></div>')
INSERT [dbo].[LAYOUT] ([layoutID], [layoutName], [layout]) VALUES (2, N'Show Blog Post', N'<p>{title}</p><p>{content}</p>')
INSERT [dbo].[LAYOUT] ([layoutID], [layoutName], [layout]) VALUES (3, N'Static Post', N'<p>{content}</p>')
SET IDENTITY_INSERT [dbo].[LAYOUT] OFF


